/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biodesk;

import java.util.ArrayList;
import java.util.List;
import br.minerva.biodunet.model.*;
import br.minerva.biodunet.service.PlasmidDrawService;

/**
 *
 * @author will
 */
public class BioDesk {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      //Cria plasmidio
        Plasmid p = new Plasmid();
        
        //Preenche o plasmidio com sequencias
        ArrayList<Feature> feats = new ArrayList<Feature>();
        feats.add(new Feature(new Long(0),new Long(10),"+","MyName"));
        feats.add(new Feature(new Long(10),new Long(12),"+","MyName"));
        feats.add(new Feature(new Long(12),new Long(14),"+","MyName"));
        feats.add(new Feature(new Long(14),new Long(16),"+","MyName"));
        feats.add(new Feature(new Long(16),new Long(18),"+","MyName"));
        feats.add(new Feature(new Long(18),new Long(20),"+","MyName"));
        feats.add(new Feature(new Long(20),new Long(22),"+","MyName"));
        feats.add(new Feature(new Long(22),new Long(24),"+","MyName"));
        feats.add(new Feature(new Long(24),new Long(26),"+","MyName"));
        feats.add(new Feature(new Long(26),new Long(28),"+","MyName"));
        feats.add(new Feature(new Long(50),new Long(81),"+","MyName"));
        feats.add(new Feature(new Long(60),new Long(91),"+","MyName"));
        feats.add(new Feature(new Long(71),new Long(101),"+","MyName"));
        feats.add(new Feature(new Long(90),new Long(150),"+","333333"));
        feats.add(new Feature(new Long(180),new Long(230),"+","MyName"));
        feats.add(new Feature(new Long(30),new Long(130),"+","MyName"));
        feats.add(new Feature(new Long(85),new Long(140),"+","99999"));
        feats.add(new Feature(new Long(85),new Long(140),"+","MyName"));
        feats.add(new Feature(new Long(110),new Long(140),"+","MyName"));
        feats.add(new Feature(new Long(110),new Long(140),"+","MyName"));
        feats.add(new Feature(new Long(150),new Long(290),"+","MyName"));
        feats.add(new Feature(new Long(300),new Long(310),"+","111111111111111"));
        feats.add(new Feature(new Long(290),new Long(330),"+","000000000000000"));
        feats.add(new Feature(new Long(313),new Long(355),"+","MyName"));
        feats.add(new Feature(new Long(357),new Long(373),"+","Duplicado"));
        feats.add(new Feature(new Long(357),new Long(373),"+","Duplicado"));
        feats.add(new Feature(new Long(357),new Long(373),"+","Duplicado"));
        feats.add(new Feature(new Long(375),new Long(395),"+","MyName"));
        feats.add(new Feature(new Long(365),new Long(410),"+","MyName"));
        feats.add(new Feature(new Long(110),new Long(140),"+","Teste"));
        feats.add(new Feature(new Long(250),new Long(300),"+","Teste Teste"));
        feats.add(new Feature(new Long(250),new Long(300),"+","Teste Teste"));
        feats.add(new Feature(new Long(245),new Long(300),"+","Teste Teste TEste"));
        Feature f = new Feature(new Long(40),new Long(300),"+","A Grande Enzima");
        f.setHighlight(true);
        feats.add(f);
        
        
        p.setFeatures(feats);
        
        p.setAccession("Plasmideo");
        //Cria instancia da classe que desenha o plasmidio
        PlasmidDrawService dplasm = new PlasmidDrawService(p);
        //Desenha o plasmidio
        dplasm.draw(3.0);
        //Salva como PNG (caminho, tipo)
        dplasm.save("imagem.png",'p');
        //Salva como JPG
        dplasm.save("imagem.jpg",'j');
    }
}
