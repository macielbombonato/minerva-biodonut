package br.minerva.biodunet.web.enums;

import br.minerva.biodunet.enums.IStringEnum;

public enum NavigationEnum implements IStringEnum {
	
	INICIO("/home.xhtml"),
	ERRO("/pages/erro/erro.xhtml"),
	
	LOGIN("/pages/admin/usuario/login.xhtml"),
	USER_LIST("/pages/admin/usuario/list.xhtml"),
	USER_INSERT("/pages/admin/usuario/new.xhtml"),
	USER_EDIT("/pages/admin/usuario/edit.xhtml"),
	USER_CHANGEPASSWORD("/pages/admin/usuario/changepassword.xhtml"),
	USER_DELETE("/pages/admin/usuario/remove.xhtml"), 
	
	PLASMID_LOAD_FILE("/pages/admin/plasmid/loadFile.xhtml"), 
	PLASMID_INSERT("/pages/admin/plasmid/new.xhtml"),
	PLASMID_DELETE("/pages/admin/plasmid/remove.xhtml"),
	PLASMID_LIST("/pages/plasmid/list.xhtml"),
	PLASMID_SHOW("/pages/plasmid/show.xhtml"),
	
	ENZYME_LOAD_FILE("/pages/admin/enzyme/loadFile.xhtml"),
	ENZYME_INSERT("/pages/admin/enzyme/new.xhtml"),
	ENZYME_BATCH_INSERT("/pages/admin/enzyme/new_batch.xhtml"),
	ENZYME_EDIT("/pages/admin/enzyme/edit.xhtml"),
	ENZYME_DELETE("/pages/admin/enzyme/remove.xhtml"),
	ENZYME_LIST("/pages/enzyme/list.xhtml"),
	ENZYME_SHOW("/pages/enzyme/show.xhtml"),
	;
	
	private String key;

	private NavigationEnum(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
}
