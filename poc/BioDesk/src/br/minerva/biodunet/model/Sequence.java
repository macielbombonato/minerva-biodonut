package br.minerva.biodunet.model;

public class Sequence extends AbstractModel {
	
	private static final long serialVersionUID = -282356250664780523L;

	private Long id;

	private Long sequenceInitialPosition;

	private String value;

	private Plasmid plasmid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Plasmid getPlasmid() {
		return plasmid;
	}

	public void setPlasmid(Plasmid plasmid) {
		this.plasmid = plasmid;
	}

	public Long getSequenceInitialPosition() {
		return sequenceInitialPosition;
	}

	public void setSequenceInitialPosition(Long sequenceInitialPosition) {
		this.sequenceInitialPosition = sequenceInitialPosition;
	}

}
