package br.minerva.biodunet.model;

import br.minerva.biodunet.enums.TipoUsuarioEnum;

public class Usuario extends AbstractModel {

	private static final long serialVersionUID = 3487465910305819379L;

	private Long id;

	private String nome;
	
	private String email;

	private String senha;

	private TipoUsuarioEnum tipoUsuario;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoUsuarioEnum getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
