package br.minerva.biodunet.model;

import java.util.ArrayList;
import java.util.List;

public class Plasmid extends AbstractModel {
	
	private static final long serialVersionUID = 3454061271372722644L;

	private Long id;
	

	private String accession;
	

	private String locus;

	private String definition;
	

	private String version;
	

	private String keywords;
	

	private String source;

	private List<Reference> references;
	
	private List<Feature> features;
	
	private List<Sequence> sequences;
        
                
        public Plasmid(){
            id = new Long(0);
            accession = "";
            locus = "";
            definition = "";
            version = "";
            keywords = "";
            source = "";
            references = new ArrayList<Reference>();
            features = new ArrayList<Feature>();
            sequences = new ArrayList<Sequence>();
        }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccession() {
		return accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}

	public String getLocus() {
		return locus;
	}

	public void setLocus(String locus) {
		this.locus = locus;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<Reference> getReferences() {
		return references;
	}

	public void setReferences(List<Reference> references) {
		this.references = references;
	}

	public List<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(List<Feature> features) {
		this.features = features;
	}

	public List<Sequence> getSequences() {
		return sequences;
	}

	public void setSequences(List<Sequence> sequences) {
		this.sequences = sequences;
	}
	
	public List<Sequence> getLongSequences() {
		List<Sequence> longSequences = null;
		if (sequences != null && !sequences.isEmpty()) {
			longSequences = new ArrayList<Sequence>();
			Sequence newSequence = null;
			int count = 0;
			for (Sequence sequence : sequences) {
				if (count == 4 || newSequence == null) {
					newSequence = new Sequence();
					newSequence.setValue("");
					
					longSequences.add(newSequence);
					
					count = 0;
				} else {
					count++;
				}
				newSequence.setSequenceInitialPosition(sequence.getSequenceInitialPosition());
				newSequence.setValue(newSequence.getValue() + sequence.getValue().toUpperCase());
			}
		}
		return longSequences;
	}
	
	public Long getSize() {
		Long size = 0L;
		
		if (sequences != null && !sequences.isEmpty()) {
			Sequence sequence = sequences.get(sequences.size() - 1);
			if (sequence != null && sequence.getSequenceInitialPosition() != null && sequence.getValue() != null) { 
				size = sequence.getSequenceInitialPosition();
			}
		}
		
		return size;
	}

}
