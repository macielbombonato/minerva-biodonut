package br.minerva.biodunet.model;

import java.util.List;

public class Enzyme extends AbstractModel {
	
	private static final long serialVersionUID = 5751401566413965861L;

	private Long id;

	private String name;

	private String accession;
	

	private String principalValue;

	private Long principalCourtPosition;

	private String complementValue;
	

	private Long complementCourtPosition;

	private List<Integer> plasmidCourtPosition;

	private List<Integer> sequenceCourtPosition;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrincipalValue() {
		return principalValue;
	}

	public void setPrincipalValue(String principalValue) {
		this.principalValue = principalValue;
	}

	public Long getPrincipalCourtPosition() {
		return principalCourtPosition;
	}

	public void setPrincipalCourtPosition(Long principalCourtPosition) {
		this.principalCourtPosition = principalCourtPosition;
	}

	public String getComplementValue() {
		return complementValue;
	}

	public void setComplementValue(String complementValue) {
		this.complementValue = complementValue;
	}

	public Long getComplementCourtPosition() {
		return complementCourtPosition;
	}

	public void setComplementCourtPosition(Long complementCourtPosition) {
		this.complementCourtPosition = complementCourtPosition;
	}

	public String getAccession() {
		return accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}

	public List<Integer> getPlasmidCourtPosition() {
		return plasmidCourtPosition;
	}

	public void setPlasmidCourtPosition(List<Integer> plasmidCourtPosition) {
		this.plasmidCourtPosition = plasmidCourtPosition;
	}

	public List<Integer> getSequenceCourtPosition() {
		return sequenceCourtPosition;
	}

	public void setSequenceCourtPosition(List<Integer> sequenceCourtPosition) {
		this.sequenceCourtPosition = sequenceCourtPosition;
	}
	
	

}
