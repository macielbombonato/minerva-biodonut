/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.minerva.biodunet.service;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import br.minerva.biodunet.model.DrawableText;

/**
 *
 * @author will
 */
public class DrawTextService {
    ArrayList<ArrayList<DrawableText>> quadrants;
    boolean waschanged;
    
    public DrawTextService(){
        quadrants = new ArrayList<ArrayList<DrawableText>>();
        quadrants.add(new ArrayList<DrawableText>());
        quadrants.add(new ArrayList<DrawableText>());
        quadrants.add(new ArrayList<DrawableText>());
        quadrants.add(new ArrayList<DrawableText>());
        waschanged = true;
    }
    
    public void AddText(String text, Double angle, Integer radius, Integer p_endradius){
       int quad = 0;
       
       Double angleAux = angle%360;
       
       if(angleAux>=0 && angleAux <= 90){
           quad = 0;
       }else if(angleAux>90 & angleAux <= 180){
           quad = 1;
       }else if(angleAux>180 & angleAux <= 270){
           quad = 2;
       }else{
           quad = 3;
       }
           
       if(quad>=0 && quad <4){
            quadrants.get(quad).add(new DrawableText(text, angle, radius, p_endradius));
       }
       
   }
    
   public void Draw(Graphics2D g, Integer p_arcs, int centerx, int centery){
       sort(quadrants.get(0),"asc");
       sort(quadrants.get(1),"desc");
       sort(quadrants.get(2),"asc");
       sort(quadrants.get(3),"desc");
       
       Font font = new Font("Courier", Font.PLAIN, 12);
       g.setFont(font);
       
       g.setPaint(Color.BLACK);
       
       int xi, yi, xf, yf, strw;
       xi = yi = xf = yf = 0;    
       //+10;
       int i = 0;
       
       FontMetrics fm = g.getFontMetrics();
              
       initialCondition(p_arcs);
       
           while(waschanged == true){
               waschanged = false; 
               radiusCorrection(fm);
           }
           
           sort(quadrants.get(0),"asc");
           sort(quadrants.get(1),"desc");
           sort(quadrants.get(2),"asc");
           sort(quadrants.get(3),"desc");

           waschanged = true; 
           while(waschanged == true){
               waschanged = false; 
               rearrangeText(fm);
           } 

       //Math.abs(Math.sin(Math.toRadians(angle)));
       for(ArrayList<DrawableText> quads: quadrants){
           g.setPaint(Color.BLACK);
           for(DrawableText text: quads){
                //distance = quads.size()Math.round(Math.sin(text.getAngle()));
                xi = (int)Math.round(Math.cos(Math.toRadians(text.getAngle()))*text.getRadius());
                yi = (int)Math.round(Math.sin(Math.toRadians(text.getAngle()))*text.getRadius());
              
                //int xsignal = (int) (Math.cos(Math.toRadians(text.getAngle()))/Math.abs(Math.cos(Math.toRadians(text.getAngle()))));
                //int ysignal = (int) (Math.sin(Math.toRadians(text.getAngle()))/Math.abs(Math.sin(Math.toRadians(text.getAngle()))));
                //xf = text.getRadius()*xsignal+(20*xsignal);//(int)Math.round(Math.cos(Math.toRadians(text.getAngle()))*(text.getRadius()+(distance*factor)+10));
                //yf = i*ysignal*30;//(int)Math.round(Math.sin(Math.toRadians(text.getAngle()))*(text.getRadius()+(distance*factor)+10));
                xf = (int)Math.round(Math.cos(Math.toRadians(text.getEndAngle()))*text.getEndRadius());
                yf = (int)Math.round(Math.sin(Math.toRadians(text.getEndAngle()))*text.getEndRadius());

                g.drawLine(centerx+xi, centery-yi, centerx+xf, centery-yf);
                
                

                strw = fm.stringWidth(text.getName());
                
                

              if(text.getAngle() > 90 && text.getAngle() <= 270){
                    strw = strw*-1;
                    g.drawString(text.getName(), centerx+xf+strw, centery-yf);
              }else{
                    g.drawString(text.getName(), centerx+xf, centery-yf);
              }
                
              
              
              g.drawLine(centerx+xf, centery-yf, centerx+xf+strw, centery-yf);
              
              i++;
           }
           i=0;
       }
   }
   
    private void initialCondition(Integer n_arcs){
       int distance = (n_arcs*8)+10;
       for(ArrayList<DrawableText> quads: quadrants){
           for(DrawableText text: quads){
               text.setEndRadius(text.getRadius()+distance);
               text.setEndAngle(text.getAngle());
           }
       }
    }
   
    private void radiusCorrection(FontMetrics fm){
        double angle11, angle21, angle22;
        double x, y;
        int strw, quad;
        boolean swap = false;
        Integer radiusaux;
        
        quad = 0;
        for(ArrayList<DrawableText> quads: quadrants){
            quad++;
            for(DrawableText i: quads){
                angle11 = i.getEndAngle();
                for(DrawableText j: quads){
                       if(!i.equals(j) && (i.getEndRadius() > j.getEndRadius())){
                           if(i.getAngle().equals(j.getAngle())){
                               j.setAngle(j.getAngle()+1);
                               j.setEndAngle(j.getEndAngle()+1);
                           }
                           
                           angle21 = j.getAngle();
                                
                           //calcula angulo do fim da string de j
                           x = Math.cos(Math.toRadians(angle21))*j.getEndRadius();
                           y = Math.sin(Math.toRadians(angle21))*j.getEndRadius();                             
                           
                           strw = fm.stringWidth(j.getName());
                           if(quad == 2 || quad == 3){
                               strw = strw*-1;
                           }
                           angle22 = Math.toDegrees(Math.atan2(y, x+strw));
                           if(angle22<0)angle22 = 360+angle22;                          
                           
                           //verifica se ha cruzamento nas linhas desenhadas
                           //para as duas strings. se tem, troca-as de lugar
                           if(quad == 1 || quad == 3){
                               if(angle11<angle21 && angle11>angle22){
                                    swap = true;
                               }else{
                                    swap = false;
                               }
                           }else{
                               if(angle11>angle21 && angle11<angle22){
                                    swap = true;
                               }else{
                                    swap = false;
                               }
                           }
                           
                           if(swap){
                               radiusaux = i.getEndRadius();
                               i.setEndRadius(j.getEndRadius());
                               j.setEndRadius(radiusaux);
                               waschanged = true;
                           }
                           
                       }
                }
            }
         }
    }
    
    private void rearrangeText(FontMetrics fm){
        Point istart, iend, jstart, jend;
        int strw, quad, sumrad;
        int count;
        double newangle;
        DrawableText i, j;
        String inter;
            
        quad = 0;
        for(ArrayList<DrawableText> quads: quadrants){
            quad++;
            for(count=0;count<(quads.size()-1);count++){
                i = quads.get(count);
                j = quads.get(count+1);
                //calcula x e y iniciais do texto i
                istart = new Point(
                  (int)Math.round(Math.cos(Math.toRadians(i.getEndAngle()))*i.getEndRadius()),
                  (int)Math.round(Math.sin(Math.toRadians(i.getEndAngle()))*i.getEndRadius()));
                
                strw = fm.stringWidth(i.getName());
                
                //calcula x e y finais do texto i
                iend = new Point(istart.x+strw,istart.y+fm.getHeight());



                   //calcula x e y da string de j
                   jstart = new Point(
                      (int)Math.round(Math.cos(Math.toRadians(j.getEndAngle()))*j.getEndRadius()),
                      (int)Math.round(Math.sin(Math.toRadians(j.getEndAngle()))*j.getEndRadius()));

                   strw = fm.stringWidth(j.getName());

                   jend = new Point(jstart.x+strw,jstart.y+fm.getHeight());
                           
                           
                           inter = intersect(istart, iend, jstart, jend);
                           sumrad=0;                           
                           if(!inter.equals("")){
                               if(quad==1 || quad==2){
                                   sumrad = 1;
                               }else if(quad==3 || quad==4){
                                   sumrad = -1;
                               }
                               int teste = (int)Math.round(Math.hypot(jstart.x,jstart.y));
                               jstart.y += sumrad;
                               
                               newangle = Math.toDegrees(Math.atan2(jstart.y, jstart.x));
                               if(newangle<0)newangle = 360+newangle; 

                               
                               
                               j.setEndAngle(newangle);
                               j.setEndRadius((int)Math.round(Math.hypot(jstart.x,jstart.y)));
                               
                               //j.setEndRadius(j.getEndRadius()+sumrad);
                                                                                
                               
                               waschanged = true;
                           }
                           
                       }
                
            }
        }
    
    
    private String intersect(Point a1, Point a2, Point b1, Point b2){
        int t = 6;
        if(      (a1.x > b1.x-t && a1.x < b2.x+t) && (a1.y > b1.y-t && a1.y < b2.y+t)){
            return "ur";
        }else if((a2.x > b1.x-t && a2.x < b2.x+t) && (a2.y > b1.y-t && a2.y < b2.y+t)){
            return "dl";
        }else if((a1.x > b1.x-t && a1.x < b2.x+t) && (a2.y > b1.y-t && a2.y < b2.y+t)){
            return "dr";
        }else if((a2.x > b1.x-t && a2.x < b2.x+t) && (a1.y > b1.y-t && a1.y < b2.y+t)){
            return "ul";
        }
        
        return "";
    }

   
   private void sort(ArrayList a,String order){
       if(order.equals("asc")){
       Collections.sort (a, new Comparator() {  
            @Override
            public int compare(Object o1, Object o2) {  
                DrawableText p1 = (DrawableText) o1;  
                DrawableText p2 = (DrawableText) o2;  
                return p1.getAngle() < p2.getAngle() ? -1 : (p1.getAngle() > p2.getAngle() ? +1 : 0);  
            }  
        });
       }else{
              Collections.sort (a, new Comparator() {  
            @Override
            public int compare(Object o1, Object o2) {  
                DrawableText p1 = (DrawableText) o1;  
                DrawableText p2 = (DrawableText) o2;  
                return p1.getAngle() > p2.getAngle() ? -1 : (p1.getAngle() < p2.getAngle() ? +1 : 0);  
            }  
        });
       }
   }
}
