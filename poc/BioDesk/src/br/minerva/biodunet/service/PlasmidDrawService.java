package br.minerva.biodunet.service;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import br.minerva.biodunet.model.*;

public class PlasmidDrawService {
          int x, y, w, h, centerx, centery, radius, arrow_size, i,level_aux, stroke, text_distance;
          double imgw, imgh, scale;
          DrawTextService texts;
          Graphics2D g2;
          BufferedImage image;
          Plasmid plasmid;
          ArrayList<Plasmid> plasmid_arcs;
          List<Color> colors;

          BasicStroke wideStroke;  
          BasicStroke thinStroke;

	public PlasmidDrawService(Plasmid plasm) {
            stroke = 10;
            wideStroke = new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
            thinStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
            this.plasmid = plasm;
            h = w = 400;
            imgw = 800;
            imgh = 800;
            x = (int)(imgw-w)/2;
            y = (int)(imgh-h)/2;
            centerx = x+w/2;
            centery = y+h/2;
            radius = w/2;  
            arrow_size = (int)stroke/3;
            colors = new ArrayList<Color>();
            
            plasmid_arcs = new ArrayList<Plasmid>();
            plasmid_arcs.add(new Plasmid());
            texts = new DrawTextService();
            //Gera cores em tons de azul
            colors.add(new Color(204,204,204));
            colors.add(new Color(102,102,102));
            colors.add(new Color(153,153,153));
            colors.add(new Color(51,51,51));
            colors.add(new Color(255,0,0));
            
            sort(plasmid.getFeatures());
            
	}

	public void draw() {
		draw(1.0);
	}

	public void draw(double scale) {
		Long plasm_size;
		Long start_position;
		Long seq_size;
		double start_angle, angle_size, middle_angle;
		Feature tmp_seq;

		imgw = imgw * scale;
		imgh = imgh * scale;

		image = new BufferedImage((int) imgw, (int) imgh, BufferedImage.TYPE_INT_RGB);
		g2 = (Graphics2D) image.createGraphics();

		g2.scale(scale, scale);

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setStroke(thinStroke);
		g2.setPaintMode();

		g2.setColor(Color.white);
		g2.fillRect(0, 0, (int) imgw, (int) imgh);

		g2.setColor(Color.black);
                
		//g2.draw(new Arc2D.Double(x, y, w, h, 0, 360, Arc2D.OPEN));
                drawMetrics();
                
		g2.setStroke(wideStroke);

		// Pega o tamanho do plasmidio
		plasm_size = 400L;//plasmid.getSize();
                
                plasmidLevelsAndColors();

		i = 0;
                for(Plasmid plasmid_level: plasmid_arcs){
                    radius = (w/2)+(i*stroke);
                    for(Feature feature: plasmid_level.getFeatures()){  
                            seq_size = feature.getSize();
                            start_position = feature.getStartPosition();
                            start_angle = start_position * 360 / plasm_size;
                            angle_size = seq_size * 360 / plasm_size;

                            middle_angle = (start_angle + (start_angle + angle_size)) / 2;

                            g2.setPaint(colors.get(feature.getColor()));
                            drawArc(start_angle, angle_size, feature.getDirection());

                            //Adiciona texto de todos as features
                            //para calcular suas posicoes depois
                            texts.AddText(feature.getName() + ": " + feature.getStartPosition()
                                            + "-" + (feature.getStartPosition() + feature.getSize()), 
                                    new Double(middle_angle),
                                    new Integer(radius),
                                    new Integer(w/2));
                    }
                    i++;
                }
                
                //Recupera tamanho original do raio
                radius = w/2;
                
                g2.setStroke(thinStroke);
                
                texts.Draw(g2,plasmid_arcs.size(),centerx,centery);

		g2.setPaint(Color.black);

		FontMetrics fm = g2.getFontMetrics();
		int strw = fm.stringWidth(plasmid.getAccession());

		g2.drawString(plasmid.getAccession(), centerx - (strw / 2), centery);

		g2.setPaint(Color.black);
		// g2.fill(new Ellipse2D.Double(x+w/2-5,y+h/2-5,10,10));

		g2.dispose();
		
	}
        
          private void drawMetrics(){
              int xi, yi, xf, yf, angle, size;
              xi = yi = xf = yf = 0;
              size = 4;

              g2.draw(new Arc2D.Double(x+stroke, y+stroke, w-stroke*2, h-stroke*2, 0, 360, Arc2D.OPEN));

              for(angle=0;angle<360;angle+=5){
                  xi = (int)Math.round(Math.cos(Math.toRadians(angle))*(radius-size-(stroke*2)));
                  yi = (int)Math.round(Math.sin(Math.toRadians(angle))*(radius-size-(stroke*2)));

                  xf = (int)Math.round(Math.cos(Math.toRadians(angle))*(radius+size-(stroke*2)));
                  yf = (int)Math.round(Math.sin(Math.toRadians(angle))*(radius+size-(stroke*2)));

                  g2.drawLine(centerx+xi, centery-yi, centerx+xf, centery-yf);

              }
          }        

	private void drawArc(double start_angle, double angle_size, String string) {            
                g2.setStroke(wideStroke);
                if(angle_size>5){
                    if("-".equals(string)){
                        g2.draw(new Arc2D.Double(centerx-radius, centery-radius, radius*2, radius*2, start_angle, angle_size-arrow_size,
                        Arc2D.OPEN)); 
                        drawArrow(start_angle+angle_size-arrow_size-0.5,string);
                    }else if("+".equals(string)){
                        g2.draw(new Arc2D.Double(centerx-radius, centery-radius, radius*2, radius*2, start_angle+arrow_size, angle_size-arrow_size,
                        Arc2D.OPEN)); 
                        drawArrow(start_angle+arrow_size+0.5,string);
                    }else{
                        g2.draw(new Arc2D.Double(centerx-radius, centery-radius, radius*2, radius*2, start_angle, angle_size,
                        Arc2D.OPEN)); 
                    }
                }else{
                   g2.draw(new Arc2D.Double(centerx-radius, centery-radius, radius*2, radius*2, start_angle, angle_size,
                   Arc2D.OPEN)); 
                }
	}

	private void drawArrow(double angle, String string) {
		g2.setStroke(wideStroke);
		// fisrt point
		int xpos1 = (int) Math.round(Math.cos(Math.toRadians(angle)) * (radius - stroke));
		int ypos1 = (int) Math.round(Math.sin(Math.toRadians(angle)) * (radius - stroke));

		int xpos2 = (int) Math.round(Math.cos(Math.toRadians(angle)) * (radius + stroke));
		int ypos2 = (int) Math.round(Math.sin(Math.toRadians(angle)) * (radius + stroke));

		int xpos3, ypos3;
		if ("-".equals(string)) {
			xpos3 = (int) Math.round(Math.cos(Math.toRadians(angle + arrow_size)) * radius);
			ypos3 = (int) Math.round(Math.sin(Math.toRadians(angle + arrow_size)) * radius);
		} else {
			xpos3 = (int) Math.round(Math.cos(Math.toRadians(angle - arrow_size)) * radius);
			ypos3 = (int) Math.round(Math.sin(Math.toRadians(angle - arrow_size)) * radius);
		}

		Polygon p = new Polygon();
		p.addPoint(centerx + xpos1, centery - ypos1);
		p.addPoint(centerx + xpos2, centery - ypos2);
		p.addPoint(centerx + xpos3, centery - ypos3);

		g2.fillPolygon(p);
	}

	public BufferedImage getImage() {
		return image;
	}
        
     private void plasmidLevelsAndColors(){
            int mypos, size, color, count, jlevel;
            Long start_aux, end_aux, start, end;
            Feature feat_prev, feat_next;

            ArrayList<Integer> adjacent_colors = new ArrayList<Integer>();
            mypos = size = color = count = jlevel = 0;
            for(Feature feat: plasmid.getFeatures()){
                            mayIAddAnArc(feat);
            }
            
            Iterator<Feature> it;
            Feature feature;
            
            for(Plasmid plasmid_level: plasmid_arcs){
                    it = plasmid_level.getFeatures().iterator();
                    while(it.hasNext()){
                            feature = it.next();  
                            
                       if(feature.isHighlight() == false){
                            if(jlevel > 0){
                                //Calcula a cor a ser usada a partir daqui
                                //Primeiro verifica adjacencia
                                for(Feature feature_aux: plasmid_arcs.get(jlevel-1).getFeatures()){
                                    start_aux = feature_aux.getStartPosition();
                                    end_aux = feature_aux.getEndPosition();

                                    start = feature.getStartPosition();
                                    end = feature.getEndPosition();
                                    
                                    if((start>=start_aux && start<=end_aux) ||
                                        (end>=start_aux && end<=end_aux) ||
                                        (start>=start_aux && end<=end_aux) ||
                                        (start<=start_aux && end>=end_aux)){

                                            adjacent_colors.add(feature_aux.getColor());
                                    }                                
                                }

                            }

                            //Coloca a feture anterior e proxima como adjacente 
                            mypos = plasmid_level.getFeatures().indexOf(feature);
                            size = plasmid_level.getFeatures().size()-1;
                            if(size>0){
                                if(mypos==0){
                                    feat_prev = plasmid_level.getFeatures().get(size);
                                    feat_next = plasmid_level.getFeatures().get(mypos+1);
                                }else if(mypos==size){
                                    feat_prev = plasmid_level.getFeatures().get(size-1);
                                    feat_next = plasmid_level.getFeatures().get(0);
                                }else{
                                    feat_prev = plasmid_level.getFeatures().get(mypos-1);
                                    feat_next = plasmid_level.getFeatures().get(mypos+1);
                                }
                                adjacent_colors.add(feat_prev.getColor());
                                adjacent_colors.add(feat_next.getColor());
                            }
                            //Em seguida, verifica a cor que nao foi usada nas adjacentes
                            //e atribui esta cor para a feature
                            for(count=3;count>=0;count--){
                                if(adjacent_colors.contains(count) == false){
                                    color = count;
                                }
                            }   //color = 0;
                            

                            //cor escolhida. Limpa a lista de cores adjacentes
                            adjacent_colors.clear();
                       }else{
                           color = 4;
                       }
                            //seta a cor na feature
                            feature.setColor(color);
                            
                }
                jlevel++;
            }
     }

    private void mayIAddAnArc(Feature f){
        mayIAddAnArc(f, 0);
        plasmid_arcs.get(level_aux).getFeatures().add(f);
    }
  
  
    private void mayIAddAnArc(Feature f, int level){
        level_aux = level;
        if(plasmid_arcs.size() < (level+1)){
            plasmid_arcs.add(new Plasmid());
        }

        Plasmid plasmid_aux = plasmid_arcs.get(level);

        Iterator<Feature> it = plasmid_aux.getFeatures().iterator();
        Feature tmp_level_seq;
        Long start, end, start_aux, end_aux;

        while(it.hasNext()){
            tmp_level_seq = it.next();

            start_aux = tmp_level_seq.getStartPosition();
            end_aux = tmp_level_seq.getStartPosition()+tmp_level_seq.getSize();

            start = f.getStartPosition();
            end = f.getEndPosition();

            if((start>=start_aux && start<end_aux) ||
                    (end>start_aux && end<=end_aux) ||
                    (start>=start_aux && end<=end_aux) ||
                    (start<=start_aux && end>=end_aux)){
                //Altera tamanho do raio
                mayIAddAnArc(f, level+1);
            }
        }
    
  }
        
          
  public void save(String path, char type){
       try {
        File file = new File(path);
        if(type == 'p'){
            //Save as PNG
            ImageIO.write(image, "png", file);
        }else if(type == 'j'){
            // Save as JPEG
            ImageIO.write(image, "jpg", file);
        }
    } catch (IOException e) {
    }
  }
  
  
     private void sort(List a){
       Collections.sort (a, new Comparator() {  
            @Override
            public int compare(Object o1, Object o2) {  
                Feature p1 = (Feature) o1;  
                Feature p2 = (Feature) o2;  
                return p1.getStartPosition() < p2.getStartPosition() ? -1 : (p1.getStartPosition() > p2.getStartPosition() ? +1 : 0);  
            }  
        });
   }
}
