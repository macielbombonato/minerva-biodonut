package br.minerva.biodunet.enums;

public enum TipoUsuarioEnum implements IIntEnum {
	
	ADM(0),
	USUARIO(1);
	
	private int key;

	private TipoUsuarioEnum(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}

}
