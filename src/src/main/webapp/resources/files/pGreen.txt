LOCUS       AJ007829                3228 bp    DNA     circular SYN 14-NOV-2006
DEFINITION  Cloning Vector pGreen.
ACCESSION   AJ007829
VERSION     AJ007829.1  GI:3402815
KEYWORDS    lacZ gene; NptI gene.
SOURCE      Cloning vector pGreen
  ORGANISM  Cloning vector pGreen
            other sequences; artificial sequences; vectors.
REFERENCE   1
  AUTHORS   Hellens,R.P., Edwards,E.A., Leyland,N.R., Bean,S. and
            Mullineaux,P.M.
  TITLE     pGreen: a versatile and flexible binary Ti vector for
            Agrobacterium-mediated plant transformation
  JOURNAL   Plant Mol. Biol. 42 (6), 819-832 (2000)
   PUBMED   10890530
REFERENCE   2  (bases 1 to 3228)
  AUTHORS   Hellens,R.P.
  TITLE     Direct Submission
  JOURNAL   Submitted (14-JUL-1998) Hellens R.P., Applied Genetics, John Innes
            Center, Colney Lane, Norwich, NR4 7UH, UK
FEATURES             Location/Qualifiers
     source          1..3228
                     /organism="Cloning vector pGreen"
                     /mol_type="other DNA"
                     /db_xref="taxon:80756"
                     /focus
     source          533..1343
                     /organism="Agrobacterium tumefaciens"
                     /mol_type="other DNA"
                     /db_xref="taxon:358"
                     /note="T-DNA"
     gene            26..521
                     /gene="pSa"
     rep_origin      26..521
                     /gene="pSa"
                     /citation=[1]
                     /direction=RIGHT
     gene            complement(578..940)
                     /gene="lacZ'"
     CDS             complement(578..940)
                     /gene="lacZ'"
                     /function="MCS"
                     /codon_start=1
                     /transl_table=11
                     /protein_id="CAA07704.1"
                     /db_xref="GI:3402817"
                     /translation="MTMITPSSELTLTKGNKSWSSTAVAAALELVDPPGCRNSISSLS
                     IPSTSRGGPVPNSPYSESYYNSLAVVLQRRDWENPGVTQLNRLAAHPPFASWRNSEEA
                     RTDRPSQQLRSLNGEWKL"
     gene            complement(2094..2909)
                     /gene="NptI"
     CDS             complement(2094..2909)
                     /gene="NptI"
                     /function="Kanamycin resistance"
                     /codon_start=1
                     /transl_table=11
                     /protein_id="CAA07703.1"
                     /db_xref="GI:3402816"
                     /translation="MSHIQRETSCSRPRLNSNMDADLYGYKWARDNVGQSGATIYGLY
                     GKPDAPELFLKHGKGSVANDVTDEMVRLNWLTEFMPLPTIKHFIRTPDDAWLLTTAIP
                     GKTAFQVLEEYPDSGENIVDALAVFLRRLHSIPVCNCPFNSDRVFRLAQAQSRMNNGL
                     VDASDFDDERNGWPVEQVWKEMHKLLPFSPDSVVTHGDFSLDNLIFDEGKLIGCIDVG
                     RVGIADRYQDLAILWNCLGEFSPSLQKRLFQKYGIDNPDMNKLQFHLMLDEFF"
ORIGIN      
        1 tttttatccc cggaagcctg tggatagagg gtagttatcc acgtgaaacc gctaatgccc
       61 cgcaaagcct tgattcacgg ggctttccgg cccgctccaa aaactatcca cgtgaaatcg
      121 ctaatcaggg tacgtgaaat cgctaatcgg agtacgtgaa atcgctaata aggtcacgtg
      181 aaatcgctaa tcaaaaaggc acgtgagaac gctaatagcc ctttcagatc aacagcttgc
      241 aaacacccct cgctccggca agtagttaca gcaagtagta tgttcaatta gcttttcaat
      301 tatgaatata tatatcaatt attggtcgcc cttggcttgt ggacaatgcg ctacgcgcac
      361 cggctccgcc cgtggacaac cgcaagcggt tgcccaccgt cgagcgccag cgcctttgcc
      421 cacaacccgg cggccggccg caacagatcg ttttataaat tttttttttt gaaaaagaaa
      481 aagcccgaaa ggcggcaacc tctcgggctt ctggatttcc gatccccgga attagatctt
      541 ggcaggatat attgtggtgt aacgttaaca ttaacgttta caatttccat tcgccattca
      601 ggctgcgcaa ctgttgggaa gggcgatcgg tgcgggcctc ttcgctatta cgccagctgg
      661 cgaaaggggg atgtgctgca aggcgattaa gttgggtaac gccagggttt tcccagtcac
      721 gacgttgtaa aacgacggcc agtgaattgt aatacgactc actatagggc gaattgggta
      781 ccgggccccc cctcgaggtc gacggtatcg ataagcttga tatcgaattc ctgcagcccg
      841 ggggatccac tagttctaga gcggccgcca ccgcggtgga gctccagctt ttgttccctt
      901 tagtgagggt taattccgag cttggcgtaa tcatggtcat agctgtttcc tgtgtgaaat
      961 tgttatccgc tcacaattcc acacaacata cgagccggaa ghcataaagt gtaaagcctg
     1021 gggtgcctaa tgagtgagct aactcacatt aattgcgttg cgctcactgc ccgctttcca
     1081 gtcgggaaac ctgtcgtgcc agctgcatta atgaatcggc caacgcgcgg ggagaggcgg
     1141 tttgcgtatt gggcgctctt ccgcttcctc gctcactgac tcgctgcgct cggtcgttcg
     1201 gctgcggcga gcggtatcag ctcactcaaa ggcggtaata cggttatcca cagaatcagg
     1261 ggataacgca ggaaagaaca tgaaggcctt gacaggatat attggcgggt aaactaagtc
     1321 gctgtatgtg tttgtttgag atctcatgtg agcaaaaggc cagcaaaagg ccaggaaccg
     1381 taaaaaggcc gcgttgctgg cgtttttcca taggctccgc ccccctgacg agcatcacaa
     1441 aaatcgacgc tcaagtcaga ggtggcgaaa cccgacagga ctataaagat accaggcgtt
     1501 tccccctgga agctccctcg tgcgctctcc tgttccgacc ctgccgctta ccggatacct
     1561 gtccgccttt ctcccttcgg gaagcgtggc gctttctcat agctcacgct gtaggtatct
     1621 cagttcggtg taggtcgttc gctccaagct gggctgtgtg cacgaacccc ccgttcagcc
     1681 cgaccgctgc gccttatccg gtaactatcg tcttgagtcc aacccggtaa gacacgactt
     1741 atcgccactg gcagcagcca ctggtaacag gattagcaga gcgaggtatg taggcggtgc
     1801 tacagagttc ttgaagtggt ggcctaacta cggctacact agaaggacag tatttggtat
     1861 ctgcgctctg ctgaagccag ttaccttcgg aaaaagagtt ggtagctctt gatccggcaa
     1921 acaaaccacc gctggtagcg gtggtttttt tgtttgcaag cagcagatta cgcgcagaaa
     1981 aaaaggatct caagaagatc ctttgatctt ttctacgggg tctgacgctc agtggaacga
     2041 aaactcacgt taagggattt tggtcatggt tacaaccaat taaccaattc tgattagaaa
     2101 aactcatcga gcatcaaatg aaactgcaat ttattcatat caggattatc aataccatat
     2161 ttttgaaaaa gccgtttctg taatgaagga gaaaactcac cgaggcagtt ccataggatg
     2221 gcaagatcct ggtatcggtc tgcgattccg actcgtccaa catcaataca acctattaat
     2281 ttcccctcgt caaaaataag gttatcaagt gagaaatcac catgagtgac gactgaatcc
     2341 ggtgagaatg gcaaaagttt atgcatttct ttccagactt gttcaacagg ccagccatta
     2401 cgctcgtcat caaaatcact cgcatcaacc aaaccgttat tcattcgtga ttgcgcctga
     2461 gcgagacgaa atacgcgatc gctgttaaaa ggacaattac aaacaggaat cgaatgcaac
     2521 cggcgcagga acactgccag cgcatcaaca atattttcac ctgaatcagg atattcttct
     2581 aatacctgga atgctgtttt ccctgggatc gcagtggtga gtaaccatgc atcatcagga
     2641 gtacggataa aatgcttgat ggtcggaaga ggcataaatt ccgtcagcca gtttagtctg
     2701 accatctcat ctgtaacatc attggcaacg ctacctttgc catgtttcag aaacaactct
     2761 ggcgcatcgg gcttcccata caatccatag attgtcgcac ctgattgccc gacattatcg
     2821 cgagcccatt tatacccata taaatcagca tccatgttgg aatttaatcg cggcctggag
     2881 caagacgttt cccgttgaat atggctcata acaccccttg tattactgtt tatgtaagca
     2941 gacagtttta ttgttcatga tgatatattt ttatcttgtg caatgtaaca tcagagattt
     3001 tgagacacaa cgtggctttg ttgaataaat cgaacttttg ctgagttgaa ggatcagatc
     3061 acgcatcttc ccgacaacgc agaccgttcc gtggcaaagc aaaagttcaa aatcaccaac
     3121 tggtccacct acaacaaagc tctcatcaac cgtggctccc tcactttctg gctggatgat
     3181 ggggcgattc aggcgatccc catccaacag cccgccgtcg agcgggct
//
