/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.minerva.biodunet.vo;

import br.minerva.biodunet.model.AbstractModel;

/**
 * 
 * @author will
 */
public class DrawableText extends AbstractModel {

	private static final long serialVersionUID = -8262828742026865796L;

	private Long id;

	private String name;

	private Double angle, endangle;

	private Integer radius, endradius;

	public DrawableText(String p_name, Double p_angle, Integer p_radius,
			Integer p_endradius) {
		setName(p_name);
		setAngle(p_angle);
		setRadius(p_radius);
		setEndAngle(p_angle);
		setEndRadius(p_endradius);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAngle() {
		return angle;
	}

	public void setAngle(Double p_angle) {
		this.angle = p_angle;
	}

	public Integer getRadius() {
		return radius;
	}

	public void setRadius(Integer p_radius) {
		this.radius = p_radius;
	}

	public Double getEndAngle() {
		return endangle;
	}

	public void setEndAngle(Double p_angle) {
		this.endangle = p_angle;
	}

	public Integer getEndRadius() {
		return endradius;
	}

	public void setEndRadius(Integer p_radius) {
		this.endradius = p_radius;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((angle == null) ? 0 : angle.hashCode());
		result = prime * result + ((radius == null) ? 0 : radius.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DrawableText other = (DrawableText) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (angle == null) {
			if (other.angle != null)
				return false;
		} else if (!angle.equals(other.angle))
			return false;
		if (radius == null) {
			if (other.radius != null)
				return false;
		} else if (!radius.equals(other.radius))
			return false;
		return true;
	}
}
