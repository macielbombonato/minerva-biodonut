/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.minerva.biodunet.vo;

import java.util.ArrayList;

import br.minerva.biodunet.model.Feature;

/**
 *
 * @author will
 */
public class Node {
    ArrayList<Node> edges;
    Feature feature;
    
    public Node(Feature f)
    {
        feature = f;
    }
    
    public void addEdge(Node n){
        edges.add(n);
    }
    
    public void removeEdge(Node n){
        edges.remove(n);
    }
}
