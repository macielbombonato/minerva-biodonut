package br.minerva.biodunet.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import br.minerva.biodunet.model.Enzyme;
import br.minerva.biodunet.model.Plasmid;

@Component("enzymeDao")
public class EnzymeDao extends AbstractDao<Enzyme> {
	
	@SuppressWarnings("unchecked")
	public List<Enzyme> list() {
		List<Enzyme> enzymes = null;
		Query query = getManager().createNamedQuery("Enzyme.getAll");
		enzymes = (List<Enzyme>) query.getResultList();
		return enzymes;
	}
	
	public Plasmid findById(long id) {
		return getManager().find(Plasmid.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Enzyme> getByName(String name) {
		List<Enzyme> enzymes = null;
		String queryStr = "select t from Enzyme t where t.name like '%?%' order by t.name ";
		queryStr = queryStr.replace("?", name);
		Query query = getManager().createQuery(queryStr);
		enzymes = (List<Enzyme>) query.getResultList();
		return enzymes;
	}
}
