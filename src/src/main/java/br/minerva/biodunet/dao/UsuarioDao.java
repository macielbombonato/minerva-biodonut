package br.minerva.biodunet.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import br.minerva.biodunet.model.Usuario;

@Component("usuarioDao")
public class UsuarioDao extends AbstractDao<Usuario> {
	
	@SuppressWarnings("unchecked")
	public List<Usuario> list() {
		List<Usuario> usuarios = null;
		Query query = getManager().createNamedQuery("Usuario.getAll");
		usuarios = (List<Usuario>) query.getResultList();
		return usuarios;
	}
	
	public Usuario getByEmail(String email) {
		Usuario usuario = null;
		Query query = getManager().createNamedQuery("Usuario.getByEmail");
		query.setParameter("email", email);
		usuario = (Usuario) query.getSingleResult();
		return usuario;
	}
	
	public long count() {
		long count = 0;
		Query query = getManager().createNamedQuery("Usuario.count");
		count = (Long) query.getSingleResult();
		return count;
	}

	public Usuario findById(long id) {
		return getManager().find(Usuario.class, id);
	}
}
