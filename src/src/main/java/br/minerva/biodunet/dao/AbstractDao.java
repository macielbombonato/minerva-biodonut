package br.minerva.biodunet.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.minerva.biodunet.model.IModel;

public abstract class AbstractDao<E extends IModel> {
	
	@PersistenceContext
	private EntityManager manager;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public E save(E entity) {
		if (entity.getId() != null) {
			manager.merge(entity);
		} else {
			manager.persist(entity);
		}
		return entity;
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public E update(E entity) {
		if (entity.getId() != null) {
			manager.merge(entity);
		} 
		return entity;
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void delete(E entity) {
		@SuppressWarnings("unchecked")
		E retrieved = (E) manager.find(entity.getClass(), entity.getId());
		if (retrieved != null) {
			manager.remove(retrieved);	
		}
	}
	
	public E findById(Class<E> clazz, long id) {
		return getManager().find(clazz, id);
	}
	
	public EntityManager getManager() {
		return manager;
	}
}
