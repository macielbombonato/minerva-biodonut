package br.minerva.biodunet.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.minerva.biodunet.model.Feature;
import br.minerva.biodunet.model.Plasmid;
import br.minerva.biodunet.model.Reference;
import br.minerva.biodunet.model.Sequence;

@Component("plasmidDao")
public class PlasmidDao extends AbstractDao<Plasmid> {
	
	@SuppressWarnings("unchecked")
	public List<Plasmid> list() {
		List<Plasmid> plasmids = null;
		Query query = getManager().createNamedQuery("Plasmid.getAll");
		plasmids = (List<Plasmid>) query.getResultList();
		return plasmids;
	}
	
	@SuppressWarnings("unchecked")
	public List<Reference> referencesList(Plasmid plasmid) {
		List<Reference> list = null;
		Query query = getManager().createNamedQuery("Reference.getAll");
		query.setParameter("plasmid_id", plasmid.getId());
		list = (List<Reference>) query.getResultList();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Feature> featuresList(Plasmid plasmid) {
		List<Feature> list = null;
		Query query = getManager().createNamedQuery("Feature.getAll");
		query.setParameter("plasmid_id", plasmid.getId());
		list = (List<Feature>) query.getResultList();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Sequence> sequencesList(Plasmid plasmid) {
		List<Sequence> list = null;
		Query query = getManager().createNamedQuery("Sequence.getAll");
		query.setParameter("plasmid_id", plasmid.getId());
		list = (List<Sequence>) query.getResultList();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Plasmid> getByAccession(String accession) {
		List<Plasmid> plasmids = null;
		String queryStr = "select p from Plasmid p where p.accession like '%?%' order by p.accession";
		queryStr = queryStr.replace("?", accession);
		Query query = getManager().createQuery(queryStr);
		plasmids = (List<Plasmid>) query.getResultList();
		return plasmids;
	}
	
	public Plasmid findById(long id) {
		return getManager().find(Plasmid.class, id);
	}
	
	@Transactional(readOnly=false)
	public Plasmid save(Plasmid obj) {
		if (obj.getId() != null) {
			getManager().merge(obj);
		} else {
			getManager().persist(obj);
		}
		
		if (obj != null) {
			persistReferences(obj);
			persistFeatures(obj);
			persistSequences(obj);
		}
		
		return obj;
	}
	
	private void persistReferences(Plasmid obj) {
		if (obj.getReferences() != null && !obj.getReferences().isEmpty()) {
			for (Reference reference : obj.getReferences()) {
				reference.setPlasmid(obj);
				if (reference.getId() != null) {
					getManager().merge(reference);
				} else {
					getManager().persist(reference);
				}
			}
		}
	}
	
	private void persistFeatures(Plasmid obj) {
		if (obj.getFeatures() != null && !obj.getFeatures().isEmpty()) {
			for (Feature feature : obj.getFeatures()) {
				feature.setPlasmid(obj);
				if (feature.getId() != null) {
					getManager().merge(feature);
				} else {
					getManager().persist(feature);
				}
			}
		}
	}
	
	private void persistSequences(Plasmid obj) {
		if (obj.getSequences() != null && !obj.getSequences().isEmpty()) {
			for (Sequence sequence : obj.getSequences()) {
				sequence.setPlasmid(obj);
				if (sequence.getId() != null) {
					getManager().merge(sequence);
				} else {
					getManager().persist(sequence);
				}
			}
		}
	}
}
