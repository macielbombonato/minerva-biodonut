package br.minerva.biodunet.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.UploadedFile;

import br.minerva.biodunet.enums.DirectionEnum;
import br.minerva.biodunet.model.Enzyme;
import br.minerva.biodunet.model.Feature;
import br.minerva.biodunet.model.Plasmid;
import br.minerva.biodunet.model.Reference;
import br.minerva.biodunet.model.Sequence;

public class FileNCBIHelper {
	
	public Plasmid plasmidFileConverter(UploadedFile file) throws IOException {
		Plasmid plasmid = new Plasmid();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputstream()));
		String str = "";
		boolean readLine = true;
		while (str != null) {
		    if (readLine) { 
		    	str = in.readLine();
		    } else {
		    	readLine = true;
		    }
		    
		    if (str != null) {
		    	String[] split = str.split(" ");
		    	
		    	if (split != null && split.length > 0) {
		    		if ("LOCUS".equals(split[0])) {
		    			plasmid.setLocus(readValue(1, split));
		    		} else if ("DEFINITION".equals(split[0])) {
		    			plasmid.setDefinition(readValue(1, split));
		    		} else if ("ACCESSION".equals(split[0])) {
		    			plasmid.setAccession(readValue(1, split));
		    		} else if ("VERSION".equals(split[0])) {
		    			plasmid.setVersion(readValue(1, split));
		    		} else if ("KEYWORDS".equals(split[0])) {
		    			plasmid.setKeywords(readValue(1, split));
		    		} else if ("SOURCE".equals(split[0])) {
		    			plasmid.setSource(readValue(1, split));
		    		} else if ("REFERENCE".equals(split[0])) {

		    			Reference reference = new Reference();
		    			
		    			reference.setOrigin(readValue(1, split));
		    			
		    			str = in.readLine();
		    			split = str.split(" ");
		    			
		    			if (split.length >= 3 && "AUTHORS".equals(split[2])) {
		        			reference.setAuthors(readValue(3, split));
		        			
		    				str = in.readLine();
		    				split = str.split(" ");
		        			
		        			while ((split.length >= 3 && "".equals(split[2])) && ("".equals(split[0]))) {
		        				reference.setAuthors(reference.getAuthors() + " " + readValue(3, split));
		        				
		        				str = in.readLine();
		        				split = str.split(" ");
		        			}
		    			} 
		    			
		    			if (split.length >= 3 && "TITLE".equals(split[2])) {
		        			reference.setTitle(readValue(3, split));
		        			
		    				str = in.readLine();
		    				split = str.split(" ");
		        			
		        			while ((split.length >= 3 && "".equals(split[2])) && ("".equals(split[0]))) {
		        				reference.setTitle(reference.getTitle() + " " + readValue(3, split));
		        				
		        				str = in.readLine();
		        				split = str.split(" ");
		        			}
		    			}
		    			
		    			if (split.length >= 3 && "JOURNAL".equals(split[2])) {
		        			reference.setJournal(readValue(3, split));
		        			
		    				str = in.readLine();
		    				split = str.split(" ");
		        			
		        			while ((split.length >= 4 && ("".equals(split[2]) && "".equals(split[3]))) && ("".equals(split[0]))) {
		        				reference.setJournal(reference.getJournal() + " " + readValue(3, split));
		        				
		        				str = in.readLine();
		        				split = str.split(" ");
		        			}
		    			}
		    			
		    			if (split.length >= 4 && "PUBMED".equals(split[3])) {
		        			reference.setPubmed(readValue(4, split));
		        			
		    				str = in.readLine();
		    				split = str.split(" ");
		    			}
		    			
		    			if (split.length >= 3 && "REMARK".equals(split[2])) {
		        			reference.setRemark(readValue(3, split));
		        			
		    				str = in.readLine();
		    				split = str.split(" ");
		    			}
		    			
						readLine = false;
						
						if (plasmid.getReferences() == null) {
							plasmid.setReferences(new ArrayList<Reference>());
						} 
						
						plasmid.getReferences().add(reference);
		    		} else if ("FEATURES".equals(split[0])) {
						str = in.readLine();
						split = str.split(" ");
						
						boolean featureReadLine = true;
						boolean hasInputFeatureName = true;
						
						String featureBaseName = "";
						
		    			while ("".equals(split[0])) {
		    				Feature feature = new Feature();
		    				Feature featureJoin = null;

		        			if (split.length >= 6 && !"".equals(split[5])) {
		        				feature.setName(split[5]);
		        				featureBaseName = split[5];
		        				
		        				String value = readValue(6, split);
		        				
		        				if (value != null) {
		        					
		        					if (value.contains("complement")) {
		        						feature.setDirection(DirectionEnum.COMPLEMENT);
		        						hasInputFeatureName = false;
		        					} else {
		        						feature.setDirection(DirectionEnum.PRINCIPAL);
		        						hasInputFeatureName = false;
		        					}
		        					
		        					if (value.contains("join")) {
		        						featureJoin = new Feature();
		        						hasInputFeatureName = false;
		        					}
		        					
		        					final String CUT_POINT = "@@";
		        					
		        					value = value.replace(".", "@");
		        					
		        					String[] values = value.split(CUT_POINT);
		        					
		        					for (int i = 0; i < values.length; i++) {
		        						values[i] = values[i].replace("join(", "");
		        						values[i] = values[i].replace("complement(", "");
		        						values[i] = values[i].replace(")", "");
		        						values[i] = values[i].replace(">", "");
		        						values[i] = values[i].replace("<", "");
		        						
		        						if (i == 0) {
		        							feature.setStartPosition(new Long(values[i].split(",")[0]));
		        						}
		        						
		        						if (i == 1) {
		        							feature.setEndPosition(new Long(values[i].split(",")[0]));
		        						}
		        						
		        						if (i == 1 && featureJoin != null) {
		        							featureJoin.setStartPosition(new Long(values[i].split(",")[1]));
		        						}
		        						
		        						if (i == 2 && featureJoin != null) {
		        							featureJoin.setEndPosition(new Long(values[i].split(",")[0]));
		        						}
		        						
		        						hasInputFeatureName = false;
									}
		        				}

		        				str = in.readLine();
		        				split = str.split(" ");
		        				String featureLine = "";
		        				String auxFeatureLine = "";
		        				
		        				while (split.length >= 6 && "".equals(split[5]) && ("".equals(split[0]))) {
		        					hasInputFeatureName = true;
		        					
		        					if (feature.getDetails() == null) {
		        						feature.setDetails("");
		        					}
		        					
		        					featureLine = readValue(6, split);
		        					
		        					try {
		        						if (featureLine.contains("/note=\"") 
		        								|| featureLine.contains("/protein_id=\"")) {
		        							
		        							if (featureLine.contains("/note=\"")) {
			        							auxFeatureLine = featureLine.substring(
																	featureLine.indexOf("/note=\""), 
																	featureLine.lastIndexOf("\"")
																);
		        							} else {
			        							auxFeatureLine = featureLine.substring(
																	featureLine.indexOf("/protein_id=\""), 
																	featureLine.lastIndexOf("\"")
																);		        								
		        							}
		        							
		        							if ("/note=".equals(auxFeatureLine)) {
		        								hasInputFeatureName = false;
		        								
			        							auxFeatureLine = featureLine.substring(
																				featureLine.indexOf("/note=\""), 
																				featureLine.length()
																			);
		        							} else if ("/protein_id=".equals(auxFeatureLine)) {
		        								hasInputFeatureName = false;
		        								
			        							auxFeatureLine = featureLine.substring(
																				featureLine.indexOf("/protein_id=\""), 
																				featureLine.length()
																			);
		        							} else {
			        							if (featureLine.contains("/note=\"")) {
			        								auxFeatureLine = featureLine.replace("/note=\"", "");
			        							} else {
			        								auxFeatureLine = featureLine.replace("/protein_id=\"", "");
			        							}
		        								
		        								hasInputFeatureName = true;
		        							}
		        						} else if(!featureLine.contains("/") && auxFeatureLine != null && !"".equals(auxFeatureLine)) {
		        							auxFeatureLine += " " + featureLine;
		        							
		        							if (auxFeatureLine.contains("/note=\"")) {
		        								auxFeatureLine = auxFeatureLine.replace("/note=\"", "");
		        							} else {
		        								auxFeatureLine = auxFeatureLine.replace("/protein_id=\"", "");
		        							}
		        							
		        							hasInputFeatureName = true;
		        						}
		        						
		        						if (hasInputFeatureName && auxFeatureLine != null && !"".equals(auxFeatureLine)) {
											
											if (auxFeatureLine.indexOf("\"") > 0) {
												auxFeatureLine = auxFeatureLine.substring(0, auxFeatureLine.indexOf("\""));
											}
											
											if (featureBaseName.equals(feature.getName())) {
												feature.setName(auxFeatureLine);	
											}
											
											auxFeatureLine = null;
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
		        					
		        					feature.setDetails(feature.getDetails() + " " + featureLine);
		        					
		            				str = in.readLine();
		            				split = str.split(" ");
		            				
		            				featureReadLine = false;
		        				}
		        			}
		        			
		        			if (featureReadLine) {
		        				str = in.readLine();
		        				split = str.split(" ");
		        				
		        				featureReadLine = true;
		        			} else {
		        				featureReadLine = true;
		        			}
		        			
		        			readLine = false;
		        			
		        			if (plasmid.getFeatures() == null) {
		        				plasmid.setFeatures(new ArrayList<Feature>());
		        			}
		        			
		        			plasmid.getFeatures().add(feature);
		        			
		        			if (featureJoin != null) {
		        				featureJoin.setDirection(feature.getDirection());
		        				featureJoin.setName(feature.getName() + " - join");
		        				featureJoin.setDetails(feature.getDetails());
		        				
		        				plasmid.getFeatures().add(featureJoin);
		        			}
		    			}
		    		} else if ("ORIGIN".equals(split[0])) {
						str = in.readLine();
						split = str.split(" ");
						
						long sequenceCount = 0;
						
		    			while ("".equals(split[0])) {
		    				Sequence sequence = null;

	        				String lineValue = readValue(split);
	        				
	        				if (lineValue != null) {
	        					
	        					String[] values = lineValue.split(" ");
	        					
	        					for (String string : values) {
	        						sequence = new Sequence();
	        						
									sequenceCount += string.length();
									
									sequence.setSequenceInitialPosition(new Long(sequenceCount));
									sequence.setValue(string);
									
				        			if (plasmid.getSequences() == null) {
				        				plasmid.setSequences(new ArrayList<Sequence>());
				        			}
				        			
				        			plasmid.getSequences().add(sequence);
								}
	        				}

            				str = in.readLine();
            				split = str.split(" ");
		    			}
		    		}
		    	}
		    }
		}
		
		return plasmid;
	}
	
	public List<Enzyme> enzymesFileConverter(UploadedFile file) throws IOException {
		List<Enzyme> enzymes = new ArrayList<Enzyme>();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputstream()));
		String str = "";
		String[] lineValues;
		
		Enzyme enzyme = null;
		while ((str = in.readLine()) != null) {
			
			lineValues = str.split(" ");
			
			if (lineValues != null && lineValues.length > 0) {
				if ("ID".equals(lineValues[0])) {
					enzyme = new Enzyme();
					enzyme.setName(lineValues[lineValues.length - 1]);
					
					enzymes.add(enzyme);
				} else if("AC".equals(lineValues[0])) {
					enzyme.setAccession(lineValues[lineValues.length - 1].replace(";", ""));
				} else if("RS".equals(lineValues[0])) {
					for (int i = 0; i < lineValues.length; i++) {
						if (i == 3) {
							enzyme.setPrincipalValue(lineValues[i].replace(",", ""));
						} else if (i == 4) {
							try {
								long courtPosition = Long.parseLong(lineValues[i].replace(";", ""));
								
								enzyme.setPrincipalCourtPosition(courtPosition);
							} catch (Exception e) {
							}
						} else if (i == 5) {
							enzyme.setComplementValue(lineValues[i].replace(",", ""));
						} else if (i == 6) {
							try {
								long courtPosition = Long.parseLong(lineValues[i].replace(";", ""));
								
								enzyme.setComplementCourtPosition(courtPosition);
							} catch (Exception e) {
							}
						}
					}
				}
			}
		}
		
		return enzymes;
	}
	
	private String readValue(String[] split) {
		String value = "";
		for (int i = 0; i < split.length; i++) {
			try {
				Integer.parseInt(split[i]);				
			} catch (Exception e) {
				value += " " + split[i];
				value = value.trim();				
			}
		}
		return value;
	}
	
	private String readValue(int startIndex, String[] split) {
		String value = "";
		for (int i = startIndex; i < split.length; i++) {
			value += " " + split[i];
			value = value.trim();
		}
		return value;
	}

}
