package br.minerva.biodunet.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.minerva.biodunet.dao.UsuarioDao;
import br.minerva.biodunet.enums.TipoUsuarioEnum;
import br.minerva.biodunet.model.Usuario;
import br.minerva.biodunet.util.Encryption;
import br.minerva.biodunet.util.MinervaUtils;
import br.minerva.biodunet.web.enums.NavigationEnum;

@ManagedBean(name="usuarioController")
@SessionScoped
public class UsuarioController extends AbstractController {
	
	private static final long serialVersionUID = -907012739863420907L;

	private final String ROOT = "root@biodunet.minerva.br";

	private final Logger log = LoggerFactory.getLogger(UsuarioController.class);
	
	private String SENHA_MESTRA = "rootfirstaccesspassword";
	
	private Usuario usuario;
	private List<Usuario> usuarios;
	
	private String password;
	
	@ManagedProperty(value="#{usuarioDao}")
	private UsuarioDao usuarioDao;
	
	public UsuarioController() {
		usuario = new Usuario();
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public List<Usuario> getUsuarios() {
		if (usuarios == null) {
			log.info("Consultando lista de usuarios");
			usuarios = usuarioDao.list();
		}
		return usuarios;
	}
	
	public String incluirUsuarioOpen() {
		try {
			usuario = new Usuario();
			
			if (TipoUsuarioEnum.ADM.equals(getUsuarioAutenticado().getTipoUsuario())) {
				return redirect(NavigationEnum.USER_INSERT);	
			} else {
				setFacesMessage(
						FacesMessage.SEVERITY_WARN, 
						MinervaUtils.getMessageBundle("usuarioMsgSemAcesso"), 
						""
					);
				
				return redirect(NavigationEnum.INICIO);
			}
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String salvarUsuario() {
		try {
			try {
				usuario.setSenha(Encryption.encrypt(password));
			} catch (Exception e) {
				log.error("Erro ao criptogafar senha", e);
			}
			
			usuarioDao.save(usuario);
			usuario = new Usuario();
			invalidarUsuarios();
			
			if (ROOT.equals(getUsuarioAutenticado().getEmail())) {
				setUsuarioAutenticado(null);
				
				setFacesMessage(
						FacesMessage.SEVERITY_INFO, 
						MinervaUtils.getMessageBundle("usuarioMsgAdmCriado"), 
						""
					);
				
				return redirect(NavigationEnum.LOGIN);
			} else {
				return redirect(NavigationEnum.USER_LIST);
			}
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String editarUsuarioOpen() {
		try {
			usuario = (Usuario) getRequestObject("usuario");
			
			return redirect(NavigationEnum.USER_EDIT);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String changepasswordOpen() {
		try {
			usuario = getUsuarioAutenticado();
			
			return redirect(NavigationEnum.USER_CHANGEPASSWORD);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String editarUsuario() {
		try {
			try {
				usuario.setSenha(Encryption.encrypt(password));
			} catch (Exception e) {
				log.error("Erro ao criptogafar senha", e);
			}
			
			usuarioDao.save(usuario);
			usuario = new Usuario();
			invalidarUsuarios();
			
			return redirect(NavigationEnum.USER_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String removerUsuarioOpen() {
		try {
			usuario = (Usuario) getRequestObject("usuario");
			
			return redirect(NavigationEnum.USER_DELETE);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String removerUsuario() {
		try {
			usuarioDao.delete(usuario);
			usuario = new Usuario();
			invalidarUsuarios();
			
			return redirect(NavigationEnum.USER_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String listarUsuariosOpen() {
		try {
			return redirect(NavigationEnum.USER_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}

	private void invalidarUsuarios() {
		usuarios = null;
	}
	
	public String autenticarUsuario() {
		try {
			boolean autenticado = false;
			
			if (ROOT.equals(usuario.getEmail())) {
				log.info("Autenticando como usuario ROOT.");
				if (usuarioDao.count() == 0) {
					if (password.equals(SENHA_MESTRA)) {
						log.info("Usuario ROOT autorizado.");
						usuario.setTipoUsuario(TipoUsuarioEnum.ADM);
						
						setUsuarioAutenticado(usuario);
						
						return incluirUsuarioOpen();
					} else {
						log.info("Autenticacao como ROOT falhou.");
						
						setFacesMessage(
								FacesMessage.SEVERITY_WARN, 
								MinervaUtils.getMessageBundle("usuarioMsgErroLogin"), 
								""
							);
						
						return redirect(NavigationEnum.LOGIN);
					}				
				} else {
					setFacesMessage(
							FacesMessage.SEVERITY_WARN, 
							MinervaUtils.getMessageBundle("usuarioMsgErroLogin"), 
							""
						);
					
					return redirect(NavigationEnum.LOGIN);
				}
			} else {
				try {
					Usuario consulta = usuarioDao.getByEmail(usuario.getEmail());
					
					if (consulta != null) {
						autenticado = consulta.getSenha().equals(Encryption.encrypt(password));
					}
					
					if (autenticado) {
						setUsuarioAutenticado(consulta);
						
						return redirect(NavigationEnum.INICIO);
					} else {
						setFacesMessage(
								FacesMessage.SEVERITY_WARN, 
								MinervaUtils.getMessageBundle("usuarioMsgErroLogin"), 
								""
							);
						
						return redirect(NavigationEnum.LOGIN);
					}
				} catch (NoResultException e) {
					log.error("autenticarUsuario ", e);
					setFacesMessage(
							FacesMessage.SEVERITY_WARN, 
							MinervaUtils.getMessageBundle("usuarioMsgUsuarioNaoEncontrado"), 
							e.toString()
						);
					return redirect(NavigationEnum.LOGIN);
				} catch (Exception e) {
					log.error("autenticarUsuario ", e);
					setFacesMessage(
							FacesMessage.SEVERITY_ERROR, 
							MinervaUtils.getMessageBundle("usuarioMsgErroLogin"), 
							e.toString()
						);
					return redirect(NavigationEnum.LOGIN);
				} catch (Throwable e) {
					log.error("autenticarUsuario ", e);
					setFacesMessage(
							FacesMessage.SEVERITY_ERROR, 
							MinervaUtils.getMessageBundle("usuarioMsgErroLogin"), 
							e.toString()
						);
					return redirect(NavigationEnum.LOGIN);
				}
			}
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public List<TipoUsuarioEnum> getTiposUsuario() {
		List<TipoUsuarioEnum> tiposUsuario = new ArrayList<TipoUsuarioEnum>();
		
		for (TipoUsuarioEnum tipoUsuario : TipoUsuarioEnum.values()) {
			tiposUsuario.add(tipoUsuario);
		}
		
		return tiposUsuario;
	}
	
	public String logOff() {
		try {
			setUsuarioAutenticado(null);
			
			return redirect(NavigationEnum.LOGIN);
		} catch (Exception e) {
			return redirectError(e);
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String openInicio() {
		try {
			return redirect(NavigationEnum.INICIO);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String loginOpen() {
		try {
			return redirect(NavigationEnum.LOGIN);
		} catch (Exception e) {
			return redirectError(e);
		}
	}

	public void setUsuarioDao(UsuarioDao usuarioDao) {
		this.usuarioDao = usuarioDao;
	}

}
