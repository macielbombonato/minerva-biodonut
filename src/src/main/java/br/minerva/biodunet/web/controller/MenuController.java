package br.minerva.biodunet.web.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.minerva.biodunet.web.enums.NavigationEnum;

@ManagedBean(name = "menuController")
@RequestScoped
public class MenuController extends AbstractController {

	private static final long serialVersionUID = -907012739863420907L;

	public String openAbout() {
		return redirect(NavigationEnum.HELP_ABOUT);
	}
	
	public String openGlossary() {
		return redirect(NavigationEnum.HELP_GLOSSARY);
	}
	
	public String openPortal() {
		return redirect(NavigationEnum.HELP_PORTAL);
	}
	
	public String openDraw() {
		return redirect(NavigationEnum.HELP_DRAW);
	}
	
	public String openResearch() {
		return redirect(NavigationEnum.HELP_RESEARCH);
	}
	
	public String openHowTo() {
		return redirect(NavigationEnum.HELP_HOWTO);
	}
	
	public String openTeam() {
		return redirect(NavigationEnum.HELP_TEAM);
	}
}
