package br.minerva.biodunet.web.converter;

import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.minerva.biodunet.model.IModel;

@FacesConverter("entityConverter")
public class EntityConverter implements Converter {

	private final String key = "minerva.shared.converter.EntityConverter";
	private final String empty = "";

	private Map<String, Object> getViewMap(FacesContext context) {
		Map<String, Object> viewMap = context.getViewRoot().getViewMap();
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Map<String, Object> idMap = (Map) viewMap.get(key);
		if (idMap == null) {
			idMap = new HashMap<String, Object>();
			viewMap.put(key, idMap);
		}
		return idMap;
	}

	public Object getAsObject(FacesContext context, UIComponent c, String value) {
		if (value.isEmpty()) {
			return null;
		}
		return getViewMap(context).get(value);
	}

	public String getAsString(FacesContext context, UIComponent c, Object value) {
		if (value == null) {
			return empty;
		}
		
		String id = "";
		
		if (value != null && ((IModel) value).getId() != null) {
			id = value.getClass() + ((IModel) value).getId().toString();
			getViewMap(context).put(id, value);			
		}
		
		return id;
	}

}
