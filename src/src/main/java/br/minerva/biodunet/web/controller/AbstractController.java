package br.minerva.biodunet.web.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.minerva.biodunet.model.Usuario;
import br.minerva.biodunet.util.MinervaUtils;
import br.minerva.biodunet.web.enums.NavigationEnum;

public abstract class AbstractController implements Serializable {
	
	private static final long serialVersionUID = 6965328813666379149L;

	private final String USUARIO_AUTENTICADO = "usuarioAutenticado";
	
	private final Logger log = LoggerFactory.getLogger(AbstractController.class);
	
	protected FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }
    
    protected HttpServletRequest getRequest() {
    	return (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
    }
    
    protected HttpServletResponse getResponse() {
    	return (HttpServletResponse) getFacesContext().getExternalContext().getResponse();
    }
	
    protected HttpSession getSession() {
        return getRequest().getSession();
    }

	protected String closeSession() {
        try {
        	getSession().setAttribute(USUARIO_AUTENTICADO, null);
            getSession().invalidate();
            return redirect(NavigationEnum.LOGIN);
        } catch (Exception e) {
        	log.error("Erro ao encerrar sessao.", e);
        	return redirect(NavigationEnum.ERRO);
        }
    }
	
	public Usuario getUsuarioAutenticado() {
		if (getSession() != null) {
			return (Usuario) getSession().getAttribute(USUARIO_AUTENTICADO);	
		} else {
			return null;
		}
	}
	
	public void setUsuarioAutenticado(Usuario usuario) {
		if (getSession() != null) {
			getSession().setAttribute(USUARIO_AUTENTICADO, usuario);	
		}
	}
	
    protected String redirect(NavigationEnum navagation) {
		try {
            return navagation.getKey() + "?faces-redirect=true";
		} catch (Exception e) {
			log.error("Erro durante redirecionamento", e);
			return NavigationEnum.ERRO.getKey() + "?faces-redirect=true";
		}
	}
    
	protected String redirectError(Exception e) {
		log.error("Erro", e);
		
        setFacesMessage(
        		FacesMessage.SEVERITY_ERROR, 
        		MinervaUtils.getMessageBundle("appMsgErroSumary"), 
        		e.toString()
        	);
        
		return redirect(NavigationEnum.ERRO);
	}

	protected void setFacesMessage(Severity severidade, String mensagem, String detalhes) {
		FacesMessage facesMsg = 
        		new FacesMessage(
        				severidade, 
        				mensagem, 
        				detalhes
        			);
		FacesContext.getCurrentInstance().addMessage("", facesMsg);
	}
	
	protected void setInfoMessage(String message, String details) {
		setFacesMessage(FacesMessage.SEVERITY_INFO, message, details);
	}
	
	protected void setWarningMessage(String message, String details) {
		setFacesMessage(FacesMessage.SEVERITY_WARN, message, details);
	}
	
	protected void setErrorMessage(String message, String details) {
		setFacesMessage(FacesMessage.SEVERITY_ERROR, message, details);
	}
	
	protected void setFatalMessage(String message, String details) {
		setFacesMessage(FacesMessage.SEVERITY_FATAL, message, details);
	}
	
	protected Object getRequestObject(String objName) {
		return getFacesContext().getExternalContext().getRequestMap().get(objName);
	}
}
