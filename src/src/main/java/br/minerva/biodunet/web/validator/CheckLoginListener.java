package br.minerva.biodunet.web.validator;

import java.io.IOException;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.minerva.biodunet.web.controller.AbstractController;
import br.minerva.biodunet.web.enums.NavigationEnum;

public class CheckLoginListener extends AbstractController implements PhaseListener {

	private final Logger log = LoggerFactory.getLogger(CheckLoginListener.class);
	
	private static final long serialVersionUID = 766806545733363400L;

	public void afterPhase(PhaseEvent event) {
		validarUsuarioAutenticado(event);
	}
	
	public void beforePhase(PhaseEvent event) {
	}

	private void validarUsuarioAutenticado(PhaseEvent event) {
		HttpServletRequest request = (HttpServletRequest) getRequest();
		
		boolean loginPage = request.getRequestURL().toString().endsWith("/login.xhtml");
		boolean adminPage = request.getRequestURL().toString().contains("/admin/");
		
		if (adminPage) {
			if(getUsuarioAutenticado() == null && !loginPage) {
				HttpServletResponse response = (HttpServletResponse) getResponse();

				try {
					response.sendRedirect(NavigationEnum.LOGIN.getKey() + "?faces-redirect=true");
				} catch (IOException e) {
					log.error("erro ao redirecionar para pagina de login.", e);

					if (response != null) {
						try {
							response.sendRedirect(NavigationEnum.ERRO.getKey() + "?faces-redirect=true");
						} catch (IOException e1) {
							log.error("erro ao redirecionar para pagina de erro.", e);
						}
					}
				}
			} else if (getUsuarioAutenticado() != null && loginPage) {
				HttpServletResponse response = (HttpServletResponse) getResponse();
				
				try {
					response.sendRedirect(NavigationEnum.INICIO.getKey() + "?faces-redirect=true");
				} catch (IOException e) {
					log.error("erro ao redirecionar para pagina inicial.", e);
					
					if (response != null) {
						try {
							response.sendRedirect(NavigationEnum.ERRO.getKey() + "?faces-redirect=true");
						} catch (IOException e1) {
							log.error("erro ao redirecionar para pagina de erro.", e);
						}
					}
				}
			}			
		}
	}

	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
