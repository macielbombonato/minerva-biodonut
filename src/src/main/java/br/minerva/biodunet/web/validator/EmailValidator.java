package br.minerva.biodunet.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import br.minerva.biodunet.util.MinervaUtils;

@FacesValidator(value = "emailValidator")
public class EmailValidator implements Validator {

	public void validate(FacesContext facesContext, UIComponent uiComponent, Object value)
			throws ValidatorException {
        //Get the component's contents and cast it to a String
	    String enteredEmail = (String)value;

        //Set the email pattern string
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");

        //Match the given string with the pattern
        Matcher m = p.matcher(enteredEmail);

        //Check whether match is found
        boolean matchFound = m.matches();

        if (!matchFound) {
            FacesMessage message = new FacesMessage();
            message.setSummary(MinervaUtils.getMessageBundle("usuarioMsgEmailInvalido"));
            message.setSeverity(FacesMessage.SEVERITY_WARN);
            throw new ValidatorException(message);
        }
	}
}
