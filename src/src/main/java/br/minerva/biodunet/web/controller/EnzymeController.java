package br.minerva.biodunet.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.UploadedFile;

import br.minerva.biodunet.dao.EnzymeDao;
import br.minerva.biodunet.helper.FileNCBIHelper;
import br.minerva.biodunet.model.Enzyme;
import br.minerva.biodunet.util.MinervaUtils;
import br.minerva.biodunet.web.enums.NavigationEnum;

@ManagedBean(name = "enzymeController")
@SessionScoped
public class EnzymeController extends AbstractController {

	private static final long serialVersionUID = -907012739863420907L;

	private Enzyme enzyme;
	private List<Enzyme> enzymes;

	@ManagedProperty(value = "#{enzymeDao}")
	private EnzymeDao enzymeDao;
	
	private UploadedFile file;
	
	private String nameSearch;
	
	private boolean disabledField;

	public EnzymeController() {
		enzyme = new Enzyme();
	}

	public Enzyme getEnzyme() {
		return enzyme;
	}

	public void setEnzyme(Enzyme enzyme) {
		this.enzyme = enzyme;
	}

	public List<Enzyme> getEnzymes() {
		if (enzymes == null) {
			enzymes = enzymeDao.list();
		}
		return enzymes;
	}

	public void setEnzymes(List<Enzyme> enzymes) {
		this.enzymes = enzymes;
	}

	public void setEnzymeDao(EnzymeDao enzymeDao) {
		this.enzymeDao = enzymeDao;
	}

	public String insertOpen() {
		try {
			disabledField = false;
			
			enzyme = new Enzyme();
			
			return redirect(NavigationEnum.ENZYME_INSERT);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String editOpen() {
		try {
			disabledField = false;
			
			enzyme = (Enzyme) getRequestObject("enzyme");
			
			if (enzyme != null) {
				return redirect(NavigationEnum.ENZYME_EDIT);
			} else {
				setFacesMessage(
						FacesMessage.SEVERITY_WARN, 
						MinervaUtils.getMessageBundle("enzymeNull"), 
						""
					);
				
				return redirect(NavigationEnum.ENZYME_LIST);
			}
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String save() {
		try {
			enzymeDao.save(this.enzyme);
			
			cleanLists();
			
			return redirect(NavigationEnum.ENZYME_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String saveBatch() {
		try {
			for (Enzyme enzyme : enzymes) {
				enzymeDao.save(enzyme);	
			}
			
			cleanLists();
			
			return redirect(NavigationEnum.ENZYME_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String delete() {
		try {
			enzymeDao.delete(this.enzyme);
			
			cleanLists();
			
			return redirect(NavigationEnum.ENZYME_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String listOpen() {
		try {
			cleanLists();
			
			return redirect(NavigationEnum.ENZYME_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	private void cleanLists() {
		enzymes = null;
		
		disabledField = false;
		
		nameSearch = null;
	}
	
	public String showOpen() {
		try {
			disabledField = true;
			
			if ((Enzyme) getRequestObject("enzyme") != null) {
				this.enzyme = (Enzyme) getRequestObject("enzyme");	
			}
			
			return redirect(NavigationEnum.ENZYME_SHOW);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String deleteOpen() {
		try {
			showOpen();
			
			return redirect(NavigationEnum.ENZYME_DELETE);
		} catch (Exception e) {
			return redirectError(e);
		}
	}

	public boolean isDisabledField() {
		return disabledField;
	}
	
	public String findByName() {
		if (nameSearch != null && nameSearch.length() > 0) {
			this.enzymes = enzymeDao.getByName(nameSearch);
		} else {
			cleanLists();
		}
		
		return redirect(NavigationEnum.ENZYME_LIST); 
	}

	public String getnameSearch() {
		return nameSearch;
	}

	public void setnameSearch(String nameSearch) {
		this.nameSearch = nameSearch;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
	
	public String loadFileOpen() {
		try {
			if (getUsuarioAutenticado() != null) {
				return redirect(NavigationEnum.ENZYME_LOAD_FILE);
			} else {
				return redirect(NavigationEnum.LOGIN);
			}

		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String loadFile() {  
        this.enzymes = new ArrayList<Enzyme>();
    	
    	try {
    		if(file != null) {
    			FileNCBIHelper helper = new FileNCBIHelper();
    			
        		this.enzymes = helper.enzymesFileConverter(file);
                
    			setFacesMessage(
    					FacesMessage.SEVERITY_INFO, 
    					MinervaUtils.getMessageBundle("loadFileMsgSuccess"), 
    					""
    				);
    			
    			return redirect(NavigationEnum.ENZYME_BATCH_INSERT);
    		} else {
    			setFacesMessage(
    					FacesMessage.SEVERITY_INFO, 
    					MinervaUtils.getMessageBundle("loadFileMsgSuccess"), 
    					""
    				);
    			
    			return redirect(NavigationEnum.ENZYME_LOAD_FILE);
    		}
        } catch (IOException e) {
			setFacesMessage(
					FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), 
					""
				);
			
			return redirectError(e);
        }
    }
}
