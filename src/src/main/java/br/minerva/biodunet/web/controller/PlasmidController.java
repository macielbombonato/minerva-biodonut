package br.minerva.biodunet.web.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.imageio.ImageIO;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import br.minerva.biodunet.enums.DirectionEnum;
import br.minerva.biodunet.helper.FileNCBIHelper;
import br.minerva.biodunet.model.Enzyme;
import br.minerva.biodunet.model.Feature;
import br.minerva.biodunet.model.Plasmid;
import br.minerva.biodunet.service.EnzymeProcessService;
import br.minerva.biodunet.service.PlasmidDrawService;
import br.minerva.biodunet.service.PlasmidService;
import br.minerva.biodunet.util.MinervaUtils;
import br.minerva.biodunet.web.enums.NavigationEnum;

@ManagedBean(name = "plasmidController")
@SessionScoped
public class PlasmidController extends AbstractController {

	private static final long serialVersionUID = -907012739863420907L;
	
	private String accessionSearch;

	private Plasmid plasmid;
	private List<Plasmid> plasmids;
	
	private List<Enzyme> electiveEnzymes;
	
	private Plasmid newPlasmid;
	
	private String nameInserted;
	private String sequenceInserted;
	
	private Enzyme initialCourtEnzymeSelected;
	private Long initialPlasmidCourtPosition;
	private Long initialSequenceCourtPosition;
	
	private Enzyme finalCourtEnzymeSelected;
	private Long finalPlasmidCourtPosition;
	private Long finalSequenceCourtPosition;
	
	private DirectionEnum newPlasmidDirection = DirectionEnum.NEUTRAL;

	@ManagedProperty(value = "#{enzymeProcessService}")
	private EnzymeProcessService enzymeProcessService;
	
	@ManagedProperty(value = "#{plasmidService}")
	private PlasmidService plasmidService;

	private UploadedFile file;
	
	private StreamedContent plasmidDunetChart;
	
	private StreamedContent newPlasmidDunetChart;

	private boolean disabledField = true;
	
	private boolean canDeleteFeature = false;
	
	public PlasmidController() {
		plasmid = new Plasmid();
	}

	public Plasmid getPlasmid() {
		return plasmid;
	}

	public void setPlasmid(Plasmid plasmid) {
		this.plasmid = plasmid;
	}

	public List<Plasmid> getPlasmids() {
		if (plasmids == null) {
			plasmids = plasmidService.getPlasmids();
		}
		return plasmids;
	}

	public String loadFileOpen() {
		try {
			plasmid = new Plasmid();
			
			if (getUsuarioAutenticado() != null) {
				return redirect(NavigationEnum.PLASMID_LOAD_FILE);
			} else {
				return redirect(NavigationEnum.LOGIN);
			}

		} catch (Exception e) {
			return redirectError(e);
		}
	}

	public String loadFile() {  
        this.plasmid = new Plasmid();
    	
    	try {
    		if(file != null) {
    			canDeleteFeature = true;
    			
    			FileNCBIHelper helper = new FileNCBIHelper();
    			
        		this.plasmid = helper.plasmidFileConverter(file);
                
    			setFacesMessage(
    					FacesMessage.SEVERITY_INFO, 
    					MinervaUtils.getMessageBundle("loadFileMsgSuccess"), 
    					""
    				);
    			
    			return redirect(NavigationEnum.PLASMID_INSERT);
    		} else {
    			setFacesMessage(
    					FacesMessage.SEVERITY_INFO, 
    					MinervaUtils.getMessageBundle("loadFileMsgSuccess"), 
    					""
    				);
    			
    			return redirect(NavigationEnum.PLASMID_LOAD_FILE);
    		}
        } catch (IOException e) {
			setFacesMessage(
					FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), 
					""
				);
			
			return redirectError(e);
        }
    }
	
	public String save() {
		try {
			this.plasmid = plasmidService.save(this.plasmid);
			
			cleanLists();
			
			return redirect(NavigationEnum.PLASMID_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public boolean isDisabledField() {
		return disabledField;
	}
	
	private void cleanLists() {
		canDeleteFeature = false;
		accessionSearch = null;
		plasmid = null;
		plasmids = null;
		electiveEnzymes = null;
		newPlasmid = null;
		sequenceInserted = null;
		initialCourtEnzymeSelected = null;
		initialPlasmidCourtPosition = null;
		initialSequenceCourtPosition = null;
		finalCourtEnzymeSelected = null;
		finalPlasmidCourtPosition = null;
		finalSequenceCourtPosition = null;
		newPlasmidDirection = null;
	}
	
	public String listOpen() {
		try {
			cleanLists();
			
			return redirect(NavigationEnum.PLASMID_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String experimentOpen() {
		try {
			return redirect(NavigationEnum.PLASMID_EXPERIMENT);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String showOpen() {
		try {
			disabledField = true;
			canDeleteFeature = false;
			
			if ((Plasmid) getRequestObject("plasmid") != null) {
				this.plasmid = (Plasmid) getRequestObject("plasmid");	
			}
			
			plasmid.setReferences(plasmidService.getReferences(plasmid));
			plasmid.setFeatures(plasmidService.getFeatures(plasmid));
			plasmid.setSequences(plasmidService.getSequences(plasmid));
			
			return redirect(NavigationEnum.PLASMID_SHOW);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String deleteOpen() {
		try {
			showOpen();
			
			return redirect(NavigationEnum.PLASMID_DELETE);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public String delete() {
		try {
			if (getUsuarioAutenticado() != null) {
				plasmidService.delete(plasmid);
				this.plasmid = new Plasmid();
				cleanLists();
			}
			
			return redirect(NavigationEnum.PLASMID_LIST);
		} catch (Exception e) {
			return redirectError(e);
		}
	}
	
	public void deleteFeature() {
		try {
			Feature feature = (Feature) getRequestObject("feature");
			
			plasmid.getFeatures().remove(feature);
		} catch (Exception e) {
			redirectError(e);
		}
	}

	public boolean isCanDeleteFeature() {
		return canDeleteFeature;
	}

	public StreamedContent getPlasmidDunetChart() {
		if (plasmid != null) {
			try {
				PlasmidDrawService drawService = new PlasmidDrawService();
				
				drawService.setPlasmid(plasmid);
				drawService.draw();
				
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(drawService.getImage(), "png", os);
				InputStream is = new ByteArrayInputStream(os.toByteArray());
				plasmidDunetChart = new DefaultStreamedContent(is);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return plasmidDunetChart;
	}
	
	public StreamedContent getNewPlasmidDunetChart() {
		if (newPlasmid != null) {
			try {
				PlasmidDrawService drawService = new PlasmidDrawService();
				
				drawService.setPlasmid(newPlasmid);
				drawService.draw();
				
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(drawService.getImage(), "png", os);
				InputStream is = new ByteArrayInputStream(os.toByteArray());
				newPlasmidDunetChart = new DefaultStreamedContent(is);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			newPlasmidDunetChart = null;
		}
		
		return newPlasmidDunetChart;
	}

	public String getaccessionSearch() {
		return accessionSearch;
	}

	public void setAccessionSearch(String accessionSearch) {
		this.accessionSearch = accessionSearch;
	}
	
	public String findByAccession() {
		if (accessionSearch != null && accessionSearch.length() > 0) {
			this.plasmids = plasmidService.findByAccession(accessionSearch);
		} else {
			cleanLists();
		}
		
		return redirect(NavigationEnum.PLASMID_LIST); 
	}

	public Plasmid getNewPlasmid() {
		return newPlasmid;
	}

	public String getSequenceInserted() {
		return sequenceInserted;
	}
	
	public int getSequenceInsertedSize() {
		int size = 0;
		
		if (this.sequenceInserted != null && this.sequenceInserted.length() > 0) {
			size = this.sequenceInserted.length();
		}
		
		return size;
	}

	public void setSequenceInserted(String sequenceInserted) {
		this.sequenceInserted = sequenceInserted;
	}
	
	public void processElectiveEnzymes() {
		this.electiveEnzymes = enzymeProcessService.processElectiveEnzymes(sequenceInserted, plasmid);
	}
	
	public List<Enzyme> getElectiveEnzymes() {
		return electiveEnzymes;
	}
	
	public void setEnzymeProcessService(EnzymeProcessService enzymeProcessService) {
		this.enzymeProcessService = enzymeProcessService;
	}

	public Enzyme getInitialCourtEnzymeSelected() {
		return initialCourtEnzymeSelected;
	}

	public void setInitialCourtEnzymeSelected(Enzyme initialCourtEnzymeSelected) {
		this.initialCourtEnzymeSelected = initialCourtEnzymeSelected;
	}

	public Enzyme getFinalCourtEnzymeSelected() {
		return finalCourtEnzymeSelected;
	}

	public void setFinalCourtEnzymeSelected(Enzyme finalCourtEnzymeSelected) {
		this.finalCourtEnzymeSelected = finalCourtEnzymeSelected;
	}
	
	public void selectecEnzymeAsInitial() {
		this.initialCourtEnzymeSelected = (Enzyme) getRequestObject("enzyme");
		
		if (this.initialCourtEnzymeSelected != null 
				&& this.initialCourtEnzymeSelected.getSequenceCourtPosition() != null
				&& this.initialCourtEnzymeSelected.getSequenceCourtPosition().size() > 0) {

			this.initialSequenceCourtPosition = this.initialCourtEnzymeSelected.getSequenceCourtPosition().get(0);
		}
	}
	
	public void selectecEnzymeAsFinal() {
		this.finalCourtEnzymeSelected = (Enzyme) getRequestObject("enzyme");
		
		if (this.finalCourtEnzymeSelected != null 
				&& this.finalCourtEnzymeSelected.getSequenceCourtPosition() != null
				&& this.finalCourtEnzymeSelected.getSequenceCourtPosition().size() > 0) {

			this.finalSequenceCourtPosition = 
					this.finalCourtEnzymeSelected.getSequenceCourtPosition().get(
							this.finalCourtEnzymeSelected.getSequenceCourtPosition().size() - 1
						);
		}
	}
	
	public List<DirectionEnum> getDirections() {
		List<DirectionEnum> directions = new ArrayList<DirectionEnum>();
		
		for (DirectionEnum direction : DirectionEnum.values()) {
			directions.add(direction);
		}
		
		return directions;
	}

	public DirectionEnum getNewPlasmidDirection() {
		return newPlasmidDirection;
	}

	public void setNewPlasmidDirection(DirectionEnum newPlasmidDirection) {
		this.newPlasmidDirection = newPlasmidDirection;
	}

	public void setPlasmidService(PlasmidService plasmidService) {
		this.plasmidService = plasmidService;
	}
	
	public String processNewPlasmid() {
		boolean hasValidationError = false;
		
		if (this.finalPlasmidCourtPosition.compareTo(this.initialPlasmidCourtPosition) < 0) {
			setWarningMessage(
					MinervaUtils.getMessageBundle("msgWarn"), 
					MinervaUtils.getMessageBundle("msgValidationErrorPlasmidPosition")
				);
			
			hasValidationError = true;
		}
		
		if (this.finalSequenceCourtPosition.compareTo(this.initialSequenceCourtPosition) < 0) {
			setWarningMessage(
					MinervaUtils.getMessageBundle("msgWarn"), 
					MinervaUtils.getMessageBundle("msgValidationErrorSequencePosition")
				);
			
			hasValidationError = true;
		}
		
		if (hasValidationError) {
			return redirect(NavigationEnum.PLASMID_EXPERIMENT);
		} else {
			this.newPlasmid = plasmidService.processNewPlasmid(
					this.plasmid, 
					this.nameInserted, 
					this.sequenceInserted, 
					this.initialCourtEnzymeSelected, 
					this.initialPlasmidCourtPosition, 
					this.initialSequenceCourtPosition, 
					this.finalCourtEnzymeSelected, 
					this.finalPlasmidCourtPosition, 
					this.finalSequenceCourtPosition, 
					this.newPlasmidDirection
				);
			
			return redirect(NavigationEnum.PLASMID_SHOW);
		}
	}

	public String getNameInserted() {
		return nameInserted;
	}

	public void setNameInserted(String nameInserted) {
		this.nameInserted = nameInserted;
	}

	public Long getInitialPlasmidCourtPosition() {
		return initialPlasmidCourtPosition;
	}

	public void setInitialPlasmidCourtPosition(Long initialPlasmidCourtPosition) {
		this.initialPlasmidCourtPosition = initialPlasmidCourtPosition;
	}

	public Long getInitialSequenceCourtPosition() {
		return initialSequenceCourtPosition;
	}

	public void setInitialSequenceCourtPosition(Long initialSequenceCourtPosition) {
		this.initialSequenceCourtPosition = initialSequenceCourtPosition;
	}

	public Long getFinalPlasmidCourtPosition() {
		return finalPlasmidCourtPosition;
	}

	public void setFinalPlasmidCourtPosition(Long finalPlasmidCourtPosition) {
		this.finalPlasmidCourtPosition = finalPlasmidCourtPosition;
	}

	public Long getFinalSequenceCourtPosition() {
		return finalSequenceCourtPosition;
	}

	public void setFinalSequenceCourtPosition(Long finalSequenceCourtPosition) {
		this.finalSequenceCourtPosition = finalSequenceCourtPosition;
	}

}
