package br.minerva.biodunet.util;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class MinervaUtils {
	
	private static final String MESSAGE_RESOURCES = "MessageResources";

	public static String getMessageBundle(final String key) {
		Locale loc = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String bundleString = MESSAGE_RESOURCES;
		
		ResourceBundle bundle = ResourceBundle.getBundle(bundleString, loc, getCurrentClassLoader(null));

		return bundle.getString(key);
	}
	
	public static String getMessageBundle(final String key, Object[] params) {
		Locale loc = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String bundleString = MESSAGE_RESOURCES;
		
		ResourceBundle bundle = ResourceBundle.getBundle(bundleString, loc, getCurrentClassLoader(params));

		return bundle.getString(key);
	}
	
	private static ClassLoader getCurrentClassLoader(Object defaultObject){
		
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		
		if(loader == null){
			loader = defaultObject.getClass().getClassLoader();
		}	
		return loader;
	}

}