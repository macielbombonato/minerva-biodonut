package br.minerva.biodunet.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.minerva.biodunet.dao.PlasmidDao;
import br.minerva.biodunet.enums.DirectionEnum;
import br.minerva.biodunet.model.Enzyme;
import br.minerva.biodunet.model.Feature;
import br.minerva.biodunet.model.Plasmid;
import br.minerva.biodunet.model.Reference;
import br.minerva.biodunet.model.Sequence;

@Component("plasmidService")
public class PlasmidService {
	
	@Autowired
	private PlasmidDao plasmidDao;
	
	public Plasmid processNewPlasmid(Plasmid plasmid, 
									String name, 
									String sequence, 
									Enzyme initialEnzyme, 
									Long initialPlasmidCourt,
									Long initialSequenceCourt,
									Enzyme finalEnzyme,
									Long finalPlasmidCourt,
									Long finalSequenceCourt,
									DirectionEnum direction) {
		Plasmid newPlasmid = new Plasmid();
		
		if (plasmid != null) {
			try {
				newPlasmid.setAccession(plasmid.getAccession() + " + " + name);
				newPlasmid.setId(plasmid.getId());
				newPlasmid.setDefinition(plasmid.getDefinition());
				newPlasmid.setKeywords(plasmid.getKeywords());
				newPlasmid.setLocus(plasmid.getLocus());
				newPlasmid.setVersion(plasmid.getVersion());
				newPlasmid.setSource(plasmid.getSource());
				
				Long initialCourt = initialPlasmidCourt;
				Long finalCourt = finalPlasmidCourt;
				
				String sequenceCourted = sequence.substring(initialSequenceCourt.intValue(), finalSequenceCourt.intValue());
				
				String plasmidSequence = plasmid.getPlasmidSequence();
				
				if (plasmidSequence != null && plasmidSequence.length() > 0) {
					String newPlasmidSequenceStart = plasmidSequence.substring(0, initialCourt.intValue());
					String newPlasmidSequenceEnd = plasmidSequence.substring(finalCourt.intValue(), plasmidSequence.length() - 1);
					
					newPlasmid.setSequences(new ArrayList<Sequence>());
					
					newPlasmid.getSequences().addAll(generateSequences(newPlasmid, newPlasmidSequenceStart, sequenceCourted, newPlasmidSequenceEnd));
				}
				
				Long plasmidSize = plasmid.getSize();
				Long newPlasmidSize = newPlasmid.getSize();
				
				newPlasmid.setFeatures(new ArrayList<Feature>());
				
				if (plasmid.getFeatures() != null) {
					Feature f = null;
					for (Feature feature : plasmid.getFeatures()) {
						f = new Feature();
						
						f.setId(feature.getId());
						f.setName(feature.getName());
						f.setDetails(feature.getDetails());
						f.setDirection(feature.getDirection());
						f.setStartPosition(feature.getStartPosition().longValue());
						f.setEndPosition(feature.getEndPosition().longValue());
						
						if (feature.getStartPosition() >= finalCourt && feature.getEndPosition() >= finalCourt) {
							f.setStartPosition(feature.getStartPosition().longValue() - (plasmidSize - newPlasmidSize));
							f.setEndPosition(feature.getEndPosition().longValue() - (plasmidSize - newPlasmidSize));
							
							newPlasmid.getFeatures().add(f);
						} else if (feature.getStartPosition() <= initialCourt && feature.getEndPosition() <= initialCourt) {
							newPlasmid.getFeatures().add(f);
						}
					}
				} 
				
				Feature sequenceFeature = new Feature();
				sequenceFeature.setName(name);
				sequenceFeature.setPlasmid(newPlasmid);
				sequenceFeature.setStartPosition(initialCourt);
				sequenceFeature.setEndPosition(initialCourt + sequenceCourted.length());
				sequenceFeature.setHighlight(true);
				sequenceFeature.setDirection(direction);
				
				newPlasmid.getFeatures().add(sequenceFeature);
				
				Feature initialEnzymeFeature = new Feature();
				initialEnzymeFeature.setName(initialEnzyme.getName());
				initialEnzymeFeature.setPlasmid(newPlasmid);
				initialEnzymeFeature.setStartPosition(initialCourt);
				initialEnzymeFeature.setEndPosition(initialCourt);
				initialEnzymeFeature.setHighlight(true);
				initialEnzymeFeature.setEnzymeFeature(true);
				initialEnzymeFeature.setDirection(DirectionEnum.NEUTRAL);
				
				newPlasmid.getFeatures().add(initialEnzymeFeature);
				
				Feature finalEnzymeFeature = new Feature();
				finalEnzymeFeature.setName(finalEnzyme.getName());
				finalEnzymeFeature.setPlasmid(newPlasmid);
				finalEnzymeFeature.setStartPosition(initialCourt + sequenceCourted.length());
				finalEnzymeFeature.setEndPosition(initialCourt + sequenceCourted.length());
				finalEnzymeFeature.setHighlight(true);
				finalEnzymeFeature.setEnzymeFeature(true);
				finalEnzymeFeature.setDirection(DirectionEnum.NEUTRAL);
				
				newPlasmid.getFeatures().add(finalEnzymeFeature);
				
			} catch(Exception e) {
				e.printStackTrace();
			}			
		}
		
		return newPlasmid;
	}
	
	private List<Sequence> generateSequences(Plasmid newPlasmid, String newPlasmidSequenceStart, String sequenceCourted, String newPlasmidSequenceEnd) {
		List<Sequence> sequences = new ArrayList<Sequence>();
		
		int actualPosition = 0;
		
		actualPosition = preencherSequencia(newPlasmidSequenceStart, sequences, actualPosition, false);
		actualPosition = preencherSequencia(sequenceCourted, sequences, actualPosition, true);
		actualPosition = preencherSequencia(newPlasmidSequenceEnd, sequences, actualPosition, false);
		
		return sequences;
	}

	private int preencherSequencia(String strSequence, List<Sequence> sequences, int actualPosition, boolean isBold) {
		Sequence sequence;
		String fragment;
		int jumpSize = 10;
		int actualInitialPosition = 0;
		
		if (sequences != null 
				&& sequences.size() > 0 
				&& sequences.get(sequences.size() - 1) != null
				&& sequences.get(sequences.size() - 1).getValue() != null
				&& sequences.get(sequences.size() - 1).getValue().replaceAll("<b>", "").replaceAll("</b>", "").trim().length() < jumpSize) {
			fragment = strSequence.substring(0, jumpSize - sequences.get(sequences.size() - 1).getValue().replaceAll("<b>", "").replaceAll("</b>", "").trim().length());
			
			if (isBold) {
				sequences.get(sequences.size() - 1).setValue(sequences.get(sequences.size() - 1).getValue() + "<b>" + fragment.trim() + "</b>");
			} else {
				sequences.get(sequences.size() - 1).setValue(sequences.get(sequences.size() - 1).getValue() + fragment.trim());
			}
			
			strSequence = strSequence.substring(fragment.length(), strSequence.length() - 1);
			
			actualPosition += fragment.length();
			
			sequences.get(sequences.size() - 1).setSequenceInitialPosition(new Long(actualPosition));
		}
		
		while (strSequence.length() > actualInitialPosition) {
			sequence = new Sequence();
			if (strSequence.length() > (actualInitialPosition + jumpSize)) {
				fragment = strSequence.substring(actualInitialPosition, actualInitialPosition + jumpSize);
				
				actualPosition += jumpSize;
				sequence.setSequenceInitialPosition(new Long(actualPosition));
				actualInitialPosition += jumpSize;
				
			} else {
				fragment = strSequence.substring(actualInitialPosition, strSequence.length() - 1);
				
				sequence.setSequenceInitialPosition(new Long(fragment.length() + actualPosition));
				
				actualInitialPosition = sequence.getSequenceInitialPosition().intValue() + 1;
				actualPosition = sequence.getSequenceInitialPosition().intValue();
			}
			
			if (isBold) {
				sequence.setValue("<b>" + fragment.trim() + "</b>");
			} else {
				sequence.setValue(fragment.trim());
			}
			
			sequences.add(sequence);
		}
		return actualPosition;
	}
	
	public List<Plasmid> getPlasmids() {
		return plasmidDao.list();
	}
	
	public Plasmid save(Plasmid plasmid) {
		return plasmidDao.save(plasmid);
	}
	
	public List<Reference> getReferences(Plasmid plasmid) {
		return plasmidDao.referencesList(plasmid);
	}
	
	public List<Feature> getFeatures(Plasmid plasmid) {
		return plasmidDao.featuresList(plasmid);
	}
	
	public List<Sequence> getSequences(Plasmid plasmid) {
		return plasmidDao.sequencesList(plasmid);
	}
	
	public void delete(Plasmid plasmid) {
		plasmidDao.delete(plasmid);
	}
	
	public List<Plasmid> findByAccession(String accessionSearch) {
		List<Plasmid> plasmids = null;

		if (accessionSearch != null && accessionSearch.length() > 0) {
			plasmids = plasmidDao.getByAccession(accessionSearch);
		} 
		
		return plasmids; 
	}

}
