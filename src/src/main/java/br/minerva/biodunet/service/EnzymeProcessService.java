package br.minerva.biodunet.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.minerva.biodunet.dao.EnzymeDao;
import br.minerva.biodunet.model.Enzyme;
import br.minerva.biodunet.model.Plasmid;

@Component("enzymeProcessService")
public class EnzymeProcessService {
	
	@Autowired
	private EnzymeDao enzymeDao;
	
	public List<Enzyme> processElectiveEnzymes(String sequenceInserted, Plasmid plasmid) {
		List<Enzyme> electiveEnzymes = null;
		
		if (sequenceInserted != null && !"".equals(sequenceInserted)) {
			
			String plasmidSequenceStr = plasmid.getPlasmidSequence();
			
			electiveEnzymes = null;
			List<Enzyme> enzymes = enzymeDao.list();
			String enzymeSearchSequence = "";
			int elective = 0;
			boolean reverse = false;
			String complementCodon = "";
			String replaceValue = "";
			String reverseComplementCodon = "";
			String reverseReplaceValue = "";
			for (Enzyme enzyme : enzymes) {
				elective = 0;
				if (enzyme.getPrincipalValue() != null 
						&& !"".equals(enzyme.getPrincipalValue())
						&& enzyme.getPrincipalValue().length() > 0) {
					
					reverse = false;
					complementCodon = "";
					replaceValue = "";
					
					if (enzyme.getComplementValue() != null
							&& !"".equals(enzyme.getComplementValue())
							&& enzyme.getComplementValue().length() > 0) {
						for (int i = 0; i < enzyme.getComplementValue().length(); i++) {
							if (enzyme.getComplementValue().charAt(i) == 'A') {
								complementCodon += "T";
							} else if (enzyme.getComplementValue().charAt(i) == 'C') {
								complementCodon += "G";
							} else if (enzyme.getComplementValue().charAt(i) == 'T') {
								complementCodon += "A";
							} else if (enzyme.getComplementValue().charAt(i) == 'G') {
								complementCodon += "C";
							}
							
							if (enzyme.getPrincipalValue().contains(complementCodon)) {
								replaceValue = complementCodon;
							}
						}
						
						reverseComplementCodon = "";
						reverseReplaceValue = "";
						
						for (int i = enzyme.getComplementValue().length() - 1; i >= 0; i--) {
							if (enzyme.getComplementValue().charAt(i) == 'A') {
								reverseComplementCodon = "T" + reverseComplementCodon;
							} else if (enzyme.getComplementValue().charAt(i) == 'C') {
								reverseComplementCodon = "G" + reverseComplementCodon;
							} else if (enzyme.getComplementValue().charAt(i) == 'T') {
								reverseComplementCodon = "A" + reverseComplementCodon;
							} else if (enzyme.getComplementValue().charAt(i) == 'G') {
								reverseComplementCodon = "C" + reverseComplementCodon;
							}
							
							if (enzyme.getPrincipalValue().contains(reverseComplementCodon)) {
								reverseReplaceValue = reverseComplementCodon;
							}
						}
						
						if (reverseReplaceValue.length() > replaceValue.length()) {
							replaceValue = reverseReplaceValue;
							reverse = true;
						}
						
						if (complementCodon != null && replaceValue != null) {
							complementCodon = complementCodon.replace(replaceValue, "").trim();
						}
					}
					
					if (reverse) {
						enzymeSearchSequence = complementCodon + enzyme.getPrincipalValue();
						
						if (complementCodon != null && complementCodon.length() > 0 && enzyme != null) {
							Long courtSize = 0L;
							
							if (enzyme.getPrincipalCourtPosition() != null) {
								courtSize = enzyme.getPrincipalCourtPosition();
							}
							
							enzyme.setPrincipalCourtPosition(courtSize + complementCodon.length());	
						}
					} else {
						enzymeSearchSequence = enzyme.getPrincipalValue() + complementCodon;	
					}
				} else {
					enzymeSearchSequence = null;
				}
				
				elective += validateElective(sequenceInserted, enzymeSearchSequence, enzyme, false);
				elective += validateElective(plasmidSequenceStr, enzymeSearchSequence, enzyme, true);
				
				if (elective == 2) {
					if (electiveEnzymes == null) {
						electiveEnzymes = new ArrayList<Enzyme>();
					}
					electiveEnzymes.add(enzyme);
				}
			}
		}
		
		return electiveEnzymes;
	}
	
	private int validateElective(String validationString, String enzymeSearchSequence, Enzyme enzyme, boolean isPlasmid) {
		int elective = 0;
		if (validationString != null
				&& enzymeSearchSequence != null
				&& validationString.contains(enzymeSearchSequence)) {

			int index = 0;
			for(int i = 0; i < validationString.length(); i++) {
				index = validationString.indexOf(enzymeSearchSequence, i);
				
				if (index == -1) {
					break;
				}
				
				if (enzyme.getPrincipalCourtPosition() != null) {
					index += enzyme.getPrincipalCourtPosition().intValue();
				}
				
				if (isPlasmid) {
					addPlasmidCourtPosition(enzyme, index);
				} else {
					addSequenceCourtPosition(enzyme, index);	
				}
				
				i = index;
			}
			
			elective++;
		}
		
		return elective;
	}
	
	private void addSequenceCourtPosition(Enzyme enzyme, int index) {
		boolean addValue = true;
		
		if (enzyme.getSequenceCourtPosition() == null) {
			enzyme.setSequenceCourtPosition(new ArrayList<Long>());
		}
		
		if (enzyme.getSequenceCourtPosition().size() > 0) {
			if (enzyme.getSequenceCourtPosition().get(enzyme.getSequenceCourtPosition().size() - 1) != null
					&& enzyme.getSequenceCourtPosition().get(enzyme.getSequenceCourtPosition().size() - 1).equals(index)) {
				addValue = false;
			}
		}
		
		if (addValue) { 
			enzyme.getSequenceCourtPosition().add(new Long(index));
		}
	}
	
	private void addPlasmidCourtPosition(Enzyme enzyme, int index) {
		boolean addValue = true;
		
		if (enzyme.getPlasmidCourtPosition() == null) {
			enzyme.setPlasmidCourtPosition(new ArrayList<String>());
		}
		
		if (enzyme.getPlasmidCourtPosition().size() > 0) {
			if (enzyme.getPlasmidCourtPosition().get(enzyme.getPlasmidCourtPosition().size() - 1) != null
					&& enzyme.getPlasmidCourtPosition().get(enzyme.getPlasmidCourtPosition().size() - 1).equals(index)) {
				addValue = false;
			}
		}
		
		if (addValue) {
			enzyme.getPlasmidCourtPosition().add(index + "");	
		}
	}
}
