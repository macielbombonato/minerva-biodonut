package br.minerva.biodunet.enums;

public enum DirectionEnum implements IIntEnum {
	
	NEUTRAL(0),
	PRINCIPAL(1),
	COMPLEMENT(2);
	
	private int key;

	private DirectionEnum(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}

}
