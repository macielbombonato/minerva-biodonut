package br.minerva.biodunet.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

@Entity
@NamedQueries({
	@NamedQuery(name="Enzyme.getAll", 
			query=" select t " +
					" from Enzyme t " +
					" order by t.name ")
})
public class Enzyme extends AbstractModel {
	
	private static final long serialVersionUID = 5751401566413965861L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private String accession;
	
	@Column
	private String principalValue;
	
	@Column
	private Long principalCourtPosition;
	
	@Column
	private String complementValue;
	
	@Column
	private Long complementCourtPosition;
	
	@Transient
	private List<String> plasmidCourtPosition;
	
	@Transient
	private List<Long> sequenceCourtPosition;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrincipalValue() {
		return principalValue;
	}

	public void setPrincipalValue(String principalValue) {
		this.principalValue = principalValue;
	}

	public Long getPrincipalCourtPosition() {
		return principalCourtPosition;
	}

	public void setPrincipalCourtPosition(Long principalCourtPosition) {
		this.principalCourtPosition = principalCourtPosition;
	}

	public String getComplementValue() {
		return complementValue;
	}

	public void setComplementValue(String complementValue) {
		this.complementValue = complementValue;
	}

	public Long getComplementCourtPosition() {
		return complementCourtPosition;
	}

	public void setComplementCourtPosition(Long complementCourtPosition) {
		this.complementCourtPosition = complementCourtPosition;
	}

	public String getAccession() {
		return accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}

	public List<String> getPlasmidCourtPosition() {
		return plasmidCourtPosition;
	}

	public void setPlasmidCourtPosition(List<String> plasmidCourtPosition) {
		this.plasmidCourtPosition = plasmidCourtPosition;
	}

	public List<Long> getSequenceCourtPosition() {
		return sequenceCourtPosition;
	}

	public void setSequenceCourtPosition(List<Long> sequenceCourtPosition) {
		this.sequenceCourtPosition = sequenceCourtPosition;
	}

}
