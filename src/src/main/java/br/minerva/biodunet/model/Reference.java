package br.minerva.biodunet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name="Reference.getAll", 
			query=" select t " +
					" from Reference t " +
					" where t.plasmid.id = :plasmid_id " +
					" order by t.origin ")
})
public class Reference extends AbstractModel {
	
	private static final long serialVersionUID = -8843473362274240196L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length=4000)
	private String origin;
	
	@Column(length=4000)
	private String authors;
	
	@Column(length=4000)
	private String title;
	
	@Column(length=4000)
	private String journal;
	
	@Column
	private String pubmed;
	
	@Column
	private String remark;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plasmid_id", nullable = false)
	private Plasmid plasmid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getJournal() {
		return journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public String getPubmed() {
		return pubmed;
	}

	public void setPubmed(String pubmed) {
		this.pubmed = pubmed;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Plasmid getPlasmid() {
		return plasmid;
	}

	public void setPlasmid(Plasmid plasmid) {
		this.plasmid = plasmid;
	}

}
