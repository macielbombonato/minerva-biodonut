package br.minerva.biodunet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Index;
import org.hibernate.validator.constraints.NotEmpty;

import br.minerva.biodunet.enums.TipoUsuarioEnum;

@Entity
@NamedQueries({
	@NamedQuery(name="Usuario.getAll", 
			query="select u from Usuario u order by u.nome "),
	@NamedQuery(name="Usuario.getById", 
			query="select u from Usuario u where u.id =:id "),
	@NamedQuery(name="Usuario.getByEmail", 
			query="select u from Usuario u where u.email =:email "),
	@NamedQuery(name="Usuario.getByNome", 
			query="select u from Usuario u where u.nome like :nome order by u.nome "),
			@NamedQuery(name="Usuario.count", 
			query="select count(u) from Usuario u ")
})
public class Usuario extends AbstractModel {

	private static final long serialVersionUID = 3487465910305819379L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length=120)
	@NotEmpty
	@Size(max=120)
	private String nome;
	
	@Column(length=120)
	@NotEmpty
	@Size(max=120)
	@Index(name = "idxUsuarioEmail")
	private String email;
	
	@Column
	@NotEmpty
	private String senha;
	
	@Column
	@Enumerated
	private TipoUsuarioEnum tipoUsuario;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoUsuarioEnum getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
