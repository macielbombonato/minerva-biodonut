package br.minerva.biodunet.model;

import java.io.Serializable;

public interface IModel extends Serializable {
	
	public Long getId();
	
	public void setId(Long id);

}
