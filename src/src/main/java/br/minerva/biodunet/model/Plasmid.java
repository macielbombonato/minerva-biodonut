package br.minerva.biodunet.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@NamedQueries({
	@NamedQuery(name="Plasmid.getAll", 
			query="select p from Plasmid p order by p.accession "),
	@NamedQuery(name="Plasmid.getById", 
			query="select p from Plasmid p where p.id =:id ")
})
public class Plasmid extends AbstractModel {
	
	private static final long serialVersionUID = 3454061271372722644L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String accession;
	
	@Column(length=4000)
	private String locus;
	
	@Column(length=4000)
	private String definition;
	
	@Column
	private String version;
	
	@Column(length=4000)
	private String keywords;
	
	@Column(length=4000)
	private String source;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "plasmid", cascade = CascadeType.REMOVE)
	private List<Reference> references;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "plasmid", cascade = CascadeType.REMOVE)
	private List<Feature> features;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "plasmid", cascade = CascadeType.REMOVE)
	private List<Sequence> sequences;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccession() {
		return accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}

	public String getLocus() {
		return locus;
	}

	public void setLocus(String locus) {
		this.locus = locus;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<Reference> getReferences() {
		return references;
	}

	public void setReferences(List<Reference> references) {
		this.references = references;
	}

	public List<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(List<Feature> features) {
		this.features = features;
	}

	public List<Sequence> getSequences() {
		return sequences;
	}

	public void setSequences(List<Sequence> sequences) {
		this.sequences = sequences;
	}
	
	@Transient
	public List<Sequence> getLongSequences() {
		List<Sequence> longSequences = null;
		if (sequences != null && !sequences.isEmpty()) {
			longSequences = new ArrayList<Sequence>();
			Sequence newSequence = null;
			int count = 0;
			for (Sequence sequence : sequences) {
				if (count == 4 || newSequence == null) {
					newSequence = new Sequence();
					newSequence.setValue("");
					
					longSequences.add(newSequence);
					
					count = 0;
				} else {
					count++;
				}
				newSequence.setSequenceInitialPosition(sequence.getSequenceInitialPosition());
				newSequence.setValue(newSequence.getValue() + sequence.getValue().toUpperCase());
			}
		}
		return longSequences;
	}
	
	@Transient
	public Long getSize() {
		Long size = 0L;
		
		if (sequences != null && !sequences.isEmpty()) {
			size = new Long(getPlasmidSequence().replaceAll("<b>", "").replaceAll("</b>", "").trim().length());
		}
		
		return size;
	}
	
	@Transient
	public String getPlasmidSequence() {
		StringBuilder sb = new StringBuilder();
		
		if (getSequences() != null && getSequences().size() > 0) {
			for (Sequence sequence : getSequences()) {
				sb.append(sequence.getValue());
			}
		}
		
		return sb.toString().toUpperCase();
	}

}
