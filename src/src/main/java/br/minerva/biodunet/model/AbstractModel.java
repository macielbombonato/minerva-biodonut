package br.minerva.biodunet.model;

public abstract class AbstractModel implements IModel {

	private static final long serialVersionUID = 1622684437881495657L;

	public abstract Long getId();

	public abstract void setId(Long id);

	@Override
	public String toString() {
		return "[getId()=" + this.getId() + ", getClass()=" + this.getClass() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!getClass().getProtectionDomain().equals(obj.getClass().getProtectionDomain()))
			return false;
		IModel other = (IModel) obj;
		if (getId() != other.getId())
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if (getId() == null) {
			setId(0L);
		}
		result = prime * result + Long.signum(getId() ^ (getId() >>> 32));
		return result;
	}
}
